package com.iroshni.urduquran15lines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;


public class SurahList extends Fragment implements OnItemClickListener {
	
	private EditText filterText = null;
	ArrayAdapter<String> adapter = null;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public View onCreateView(final LayoutInflater inflater,
       final ViewGroup container, Bundle savedInstanceState) {
    	
        RelativeLayout layout = new RelativeLayout(inflater.getContext());
		layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		layout.setGravity(Gravity.CENTER);
		
		filterText = new EditText(inflater.getContext());
		filterText.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
        filterText.setMinLines(1);
        filterText.setMaxLines(1);
		filterText.addTextChangedListener(filterTextWatcher);
		adapter = new CustomAdapter<String>(inflater.getContext(),R.layout.list_item, Arrays.asList(surahName_array));
		filterText.setHint("Type here to search a Surah name");
/*		InputFilter filter = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) { 
	                for (int i = start; i < end; i++) { 
	                        if(! ( (source.charAt(i) >= 'a' && source.charAt(i) <= 'z') ||(source.charAt(i) >= 'A' && source.charAt(i) <= 'Z') || (source.charAt(i) >= '0' && source.charAt(i) <= '9')))
	                        {
	                        	    Toast.makeText(inflater.getContext(), "Enter Alphanumerics (A-Z,0-9)", Toast.LENGTH_SHORT).show();
	                                return ""; 
	                        } 
	                } 
	                
	                return null; 
	        }			
	}; 

		filterText.setFilters(new InputFilter[]{filter}); 
 */
		ListView listView = new ListView(inflater.getContext());
		listView.setOnItemClickListener(this);
		listView.setAdapter(adapter);
		listView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		//listView.setCacheColorHint(0);

	    LinearLayout ll = new LinearLayout(inflater.getContext());  
	    ll.setOrientation(LinearLayout.VERTICAL);  
	    ll.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));  
	    ll.setGravity(Gravity.CENTER);  
	    ll.setFocusable(true);
	    ll.setFocusableInTouchMode(true);
	    
	    ll.addView(filterText);  
	    ll.addView(listView);  
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		layout.addView(ll, params);
		
		//setContentView(layout);
		//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		AwesomePagerActivity.index_activity_response = -1;
		
		return layout;
    }
    
	@Override 
	public void onResume()
	{
		super.onResume();
		AwesomePagerActivity.index_activity_response = -1;
	}
	
	public void onItemClick(AdapterView arg0, View v, int position, long arg3) {
		//String item = ((android.widget.TextView)v).getText().toString();
        String item = ((android.widget.TextView)v.findViewById(R.id.name_eng)).getText().toString();
		AwesomePagerActivity.index_activity_response= surahIndex[Integer.parseInt(item.substring(0,item.indexOf(':')))-1];
		//Log.d("onItemClick",item+"@"+AwesomePagerActivity.index_activity_response);
		
		getActivity().finish();		
	}    
	
    static String surahName_array[]= {"1:  Al-Fatiha","2:  Al-Baqarah","3:  Aal-Imran","4:  An-Nisaa","5:  Al-Maidah","6:  Al-An\'am","7:  Al-A\'raf","8:  Al-Anfal","9:  At-Tawba / al-Bara\'ah","10:  Yunus",
            "11:  Hud","12:  Yusuf","13:  Ar-Ra\'d","14:  Ibrahim","15:  Al-Hijr","16:  An-Nahl","17:  Al-Israa / Bani Israil","18:  Al-Kahf","19:  Maryam","20:  Ta-Ha",
            "21:  Al-Anbiyaa","22:  Al-Hajj","23:  Al-Muminun","24:  An-Nur","25:  Al-Furqan","26:  Ash-Shu\'araa","27:  An-Naml","28:  Al-Qasas","29:  Al-\'Ankabut","30:  Ar-Rum",
            "31:  Luqman","32:  As-Sajda / al-Malaikah","33:  Al-Ahzab","34:  Saba","35:  Fatir","36:  Ya-sin","37:  As-Saffat","38:  Sad","39:  Az-Zumar","40:  Gafir / al-Mumin",
            "41:  Fussilat / Ha Mim Sajdah","42:  Ash-Shura","43:  Az-Zukhruf","44:  Ad-Dukhan","45:  Al-Jathiya","46:  Al-Ahqaf","47:  Muhammad / al-Qital","48:  Al-Fath","49:  Al-Hujurat","50:  Qaf",
            "51:  Az-Zariyat / al-Dhariyat","52:  At-Tur","53:  An-Najm","54:  Al-Qamar","55:  Ar-Rahman","56:  Al-Waqi\'a","57:  Al-Hadid","58:  Al-Mujadila","59:  Al-Hashr","60:  Al-Mumtahana",
            "61:  As-Saff","62:  Al-Jumu\'a","63:  Al-Munafiqun","64:  At-Tagabun","65:  At-Talaq","66:  At-Tahrim","67:  Al-Mulk","68:  Al-Qalam","69:  Al-Haqqa","70:  Al-Ma\'arij",
            "71:  Nuh","72:  Al-Jinn","73:  Al-Muzzammil","74:  Al-Muddaththir","75:  Al-Qiyamat","76:  Al-Insan / ad-Dahr","77:  Al-Mursalat","78:  An-Nabaa","79:  An-Nazi\'at","80:  \'Abasa",
            "81:  At-Takwir","82:  Al-Infitar","83:  Al-Mutaffifeen / at-Tatfif","84:  Al-Inshiqaq","85:  Al-Buruj","86:  At-Tariq","87:  Al-A\'la","88:  Al-Gashiya","89:  Al-Fajr","90:  Al-Balad",
            "91:  Ash-Shams","92:  Al-Lail","93:  Ad-Dhuha","94:  Al-Sharh","95:  At-Tin","96:  Al-\'Alaq","97:  Al-Qadr","98:  Al-Baiyina","99:  Al-Zalzalah","100:  Al-\'Adiyat",
            "101:  Al-Qari\'a","102:  At-Takathur","103:  Al-\'Asr","104:  Al-Humaza","105:  Al-Fil","106:  Quraish","107:  Al-Ma\'un","108:  Al-Kauthar","109:  Al-Kafirun","110:  An-Nasr",
            "111:  Al-Masad / Al-Lahab / Tabbat","112:  Al-Ikhlas / at-Tauhid","113:  Al-Falaq","114:  An-Nas"
             };

    static String su[] = {"الفاتحة", "البقرة", "اٰلِ عمران", "النساء", "المائدة", "الانعام", "الاعراف", "الانفال", "التوبة", "يونس", "هود", "يوسف", "الرّعد", "ابراهيم", "الحِجْر", "النحل", " بنی اسرائیل", "الكهف", "مريم", "طه", "الانبياء", "الحج", "المؤمنون", "النّور", "الفرقان", "الشعراء", "النمل", "القصص", "العنكبوت", "الرّوم", "لقمٰن", "السجدة", "الاحزاب", "سبا", "فاطر", "يٰس", "الصفت", "ص", "الزمر", "المومن", "حٰم السجدۃ", "الشورىٰ", "الزخرف", "الدخان", "الجاثية", "الاحقاف", "محمّـد", "الفتح", "الحُـجُـرات", "ق", "الذّٰريات", "الـطور", "الـنجـم", "الـقمـر", "الـرحـمٰـن", "الواقعہ", "الحـديد", "الـمجادلـة", "الـحـشـر", "الـمـمـتـحنة", "الـصـف", "الـجـمـعـة", "الـمنٰفقون", "الـتغابن", "الـطلاق", "الـتحريم", "الـملك", "الـقـلـم", "الـحاقّـة", "الـمعارج", "نوح", "الجن", "الـمـزمـل", "الـمّـدّثّـر", "الـقـيٰمـة", "الانسان/الدھر", "الـمرسلٰت", "الـنبا", "الـنّٰزعت", "عبس", "التكوير", "الانفطار", "المطـفـفين", "الانشقاق", "البروج", "الـطارق", "الاعـلٰى", "الغاشـيـة", "الفجر", "الـبلد", "الـشـمـس", "اللـيـل", "الضـحٰى", "الم نـشرح", "الـتين", "الـعلق", "الـقدر", "الـبينة", "الـزلزال", "الـعدٰيٰت", "الـقارعـة", "الـتكاثر", "الـعصر", "الـهمزة", "الـفيل", "قريش", "المـاعون", "الـكوثر", "الـكافرون", "الـنصر", "اللـھب", "الاخلاص", "الـفلق", "الـناس"};

    static short[] surahIndex = { 2, 3, 51, 78, 108, 129, 152, 178, 188, 209, 222, 236, 250, 256, 262, 268, 283, 294, 306, 313, 323, 332, 343, 351, 360, 367, 377, 386, 397, 405, 412, 416, 419, 429, 435, 441, 446, 453, 459, 468, 478, 484, 490, 496, 499, 503, 507, 512, 516, 519, 521, 524, 527, 529, 532, 535, 538, 543, 546, 550, 552, 554, 555, 557, 559, 561, 563, 565, 568, 570, 572, 574, 577, 579, 581, 583, 585, 587, 588, 590, 591, 592, 593, 595, 596, 597, 598, 598, 599, 601, 601, 602, 603, 603, 604, 604, 605, 605, 606, 606, 607, 607, 608, 608, 608, 609, 609, 609, 609, 610, 610, 610, 611, 611 };
    
    private TextWatcher filterTextWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                int count) {
            adapter.getFilter().filter(s);
        }

    };
}

class CustomAdapter<T> extends ArrayAdapter<T> implements Filterable{

    private List<T> mObjects;
    private Filter newFilter;
    List<T> mOrignalObjects;
	Context context;
	Typeface custom_font;

    public CustomAdapter(Context context, int textViewResourceId, List<T> objects) {
        super(context, textViewResourceId, objects);

        mOrignalObjects = objects;
        mObjects = objects;
        this.context = context;
		custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/hussaini_nastaleeq.ttf");
    }

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder();
            holder.textViewEng = (TextView) convertView.findViewById(R.id.name_eng);
            holder.textViewUrdu = (TextView) convertView.findViewById(R.id.name_urdu);
            holder.textViewUrdu.setTypeface(custom_font);
            // Bind the data efficiently with the holder.
            convertView.setTag(holder);
        } else
        {
            // Get the ViewHolder back to get fast access to the TextViews
            holder = (ViewHolder) convertView.getTag();
        }
        String engName = mObjects.get(position).toString();
        holder.textViewEng.setText(engName);
        holder.textViewUrdu.setText(SurahList.su[Integer.parseInt(engName.substring(0,engName.indexOf(':')))-1]);

		return convertView;
	}

    static class ViewHolder {
        TextView textViewEng;
        TextView textViewUrdu;
    }

    @Override
    public int getCount ()
    {
    	//return mObjects.size();
    	return mObjects.size();
    }
    
    @Override
    public T getItem (int position)
    {
    	return mObjects.get(position);
    }
   
    @Override
    public Filter getFilter() {

        if(newFilter == null) {
            newFilter = new Filter() {
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                	mObjects = (List<T>) results.values;
                    notifyDataSetChanged();
                }


            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                constraint = constraint.toString().toLowerCase();

                LinkedList<String> filteredList = new LinkedList<String>();

                for(int i=0; i<mOrignalObjects.size(); i++) {
                    
                    if(searchKeywords[i].contains(constraint)) {
                    	filteredList.add((String) mOrignalObjects.get(i));
                    }
                }

                FilterResults newFilterResults = new FilterResults();
                newFilterResults.count = filteredList.size();
                newFilterResults.values = filteredList;
                return newFilterResults;
            }
        };
    }
    return newFilter;
}

    static String[] searchKeywords = {
            "١ 1	alfatihah alfatehah  الفاتحة الفاتحہ",
            "٢ 2	albaqarah albakarah البقرة البقرہ",
            "٣ 3	alimran  alimran alimran ال عمران",
            "٤ 4	annisa  alnisa alnisa  النساء النساء",
            "٥ 5	almaidah almaida  المائدہ المائدة",
            "٦ 6	alanaam alannam alanam  الانعام الانعام",
            "٧ 7	alaraf  الاعراف الاعراف",
            "٨ 8	alanfal  الانفال الانفال",
            "٩ 9	attaubah albaraah altawbah albaraat التوبہ التوبة",
            "١٠ 10	yunus yonus yonos  یونس يونس",
            "١١ 11	hud hood  ھود هود ہود",
            "١٢ 12	yusuf  yosuf yosof  یوسف يوسف",
            "١٣ 13	alrad arrad الرعد الرعد",
            "١٤ 14	ibrahim  ابرھیم ابراہیم ابراهيم",
            "١٥ 15	alhijr الحجر الحجر",
            "١٦ 16	annahl  alnahl  النحل النحل",
            "١٧ 17	alisra  baniisrail baniisraeel بنی اسرائیل",
            "١٨ 18	alkahf الکھف الکہف الكهف",
            "١٩ 19	maryam مريم مریم",
            "٢٠ 20	ta ha taha طه طہ",
            "٢١ 21	alanbiya  الانبیاء الانبياء",
            "٢٢ 22	alhajj الحج الحج",
            "٢٣ 23	almuminun almumenoon almuminin  المومنون المؤمنون",
            "٢٤ 24	annur annoor alnur  النور النور",
            "٢٥ 25	alfurqan  الفرقان الفرقان",
            "٢٦ 26	ashshuara alshuara  الشعرا الشعراء",
            "٢٧ 27	annaml  alnaml  النمل النمل",
            "٢٨ 28	alqasas  القصص القصص",
            "٢٩ 29	alankaboott alankabut  العنکبوت العنكبوت",
            "٣٠ 30	arroom alrum arrum alroom  الروم الروم",
            "٣١ 31	luqman lukman لقمان لقمن لقمن",
            "٣٢ 32	assajdah almalaikah alsajdah  السجدہ السجدة",
            "٣٣ 33	alahzab  الاحزاب الاحزاب",
            "٣٤ 34	saba alsaba  سبا سبا",
            "٣٥ 35	fatir  alfatir فاطر فاطر",
            "٣٦ 36	yasin yaseen يس یاسین",
            "٣٧ 37	assaaffat alsaffat assaffat الصفت الصافات",
            "٣٨ 38	sad  alsad ص ص",
            "٣٩ 39	azzumar  alzumar الزمر الزمر",
            "٤٠ 40	ghafir  almomin almumin المومن المومن غافر",
            "٤١ 41	sajdah  hamim fussilat hameem السجدہ حم السجدۃ فصلت",
            "٤٢ 42	alshura ashshura  الشوری الشورى",
            "٤٣ 43	alzukhruf azzukhruf  الزؓخرف الزخرف",
            "٤٤ 44	aldukhan addukhan  الدخان الدخان",
            "٤٥ 45	aljathiyah aljathiya  الجاثیہ الجاثية",
            "٤٦ 46	alahqaf الاحقاف الاحقاف",
            "٤٧ 47	muhammad alqital mohammad محمد محمد",
            "٤٨ 48	alfath الفتح الفتح",
            "٤٩ 49	alhujurat alhujraat الحـجـرات الحجرات",
            "٥٠ 50	qaf kaf ق ق",
            "٥١ 51	adhdhariyat azzariyat aldhariyat addhariyat  الذریات الذريات",
            "٥٢ 52	attur altur  الطور الـطور",
            "٥٣ 53	annajm alnajm  النجم الـنجـم",
            "٥٤ 54	alqamar الـقمـر القمر",
            "٥٥ 55	alrahman arrahman الرحمن الـرحـمـن الرحمان",
            "٥٦ 56	alwaqia alwaqiah  الواقعہ الواقعہ",
            "٥٧ 57	alhadid  alhadeed  الحدید الحـديد",
            "٥٨ 58	almujadila almujadalah almujadilahالمجادلہ الـمجادلـة",
            "٥٩ 59	alhashr  الحشر الـحـشـر",
            "٦٠ 60	almumtahinah almumtahina almumtahanah الممتحنہ الـمـمـتـحنة",
            "٦١ 61	alsaff assaff الصف الـصـف",
            "٦٢ 62	aljumuah aljumua aljumah الجمعہ الـجـمـعـة",
            "٦٣ 63	almunafiqun almunafiqoon  المنافقون الـمنفقون المنافقون",
            "٦٤ 64	altaghabun attaghabun التغابن الـتغابن",
            "٦٥ 65	attalaq  altalaq attalak الطلاق الـطلاق",
            "٦٦ 66	attahrim  altahrim attahreem  التحریم الـتحريم",
            "٦٧ 67	almulk  almolk الملک الـملك",
            "٦٨ 68	alqalam  alkalam القلم الـقـلـم",
            "٦٩ 69	alhaqqah alhakkah الـحاقـةالحاقہ",
            "٧٠ 70	almaarij almaareej  المعارج الـمعارج",
            "٧١ 71	nuh nooh نوح نوح",
            "٧٢ 72	aljinn الجن الجن",
            "٧٣ 73	almuzammil almuzzammil  المزمل الـمـزمـل",
            "٧٤ 74	almudathir almudaththir almuddaththir المدثر الـمـدثر",
            "٧٥ 75	alqiyamah alkiyamah القیامہ الـقـيمـة",
            "٧٦ 76	alinsan addahr aldahr alinsan الانسان الانسان الدھر",
            "٧٧ 77	almursalat المرسلت الـمرسلت",
            "٧٨ 78	alnaba annaba النبا الـنبا",
            "٧٩ 79	alnaziat annaziat النزعت الـنزعت",
            "٨٠ 80	abasa عبس عبس",
            "٨١ 81	altakwir attakwir التکویر التكوير",
            "٨٢ 82	alinfitar الانفظار الانفطار",
            "٨٣ 83	almutaffifin attatfif المفطففین المطـفـفين",
            "٨٤ 84	alinshiqaq الانشقاق الانشقاق",
            "٨٥ 85	alburuj alburooj البروج البروج",
            "٨٦ 86	altariq attariq الـطارق الطارق",
            "٨٧ 87	alala الاعـلى الاعلی",
            "٨٨ 88	alghashiyah الغاشـيـة الغاشیہ",
            "٨٩ 89	alfajr alfjr  الفجرالفجر",
            "٩٠ 90	albalad  البلد الـبلد",
            "٩١ 91	alshams ashshams  الشمس الـشـمـس",
            "٩٢ 92	allayl allail اللیل اللـيـل",
            "٩٣ 93	addhuha alduha adduha الضحی الضـحى",
            "٩٤ 94	alinshirah alinshira ashsharh  الم نشرح الم نـشرح",
            "٩٥ 95	altin attin atteen alteen الـتين التین",
            "٩٦ 96	alalaq alalak  العلق الـعلق",
            "٩٧ 97	alqadr alkadr  القدر الـقدر",
            "٩٨ 98	albaiyinah  albayyinah البینہ الـبينة",
            "٩٩ 99	azzalzalah alzalzalah alzilzal  الزلزال الـزلزال",
            "١٠٠ 100	aladiyat  العدیات العدیت الـعديت",
            "١٠١ 101	alqariah alkariah الـقارعـة القارعہ",
            "١٠٢ 102	altakathur attakathur altakasur الـتكاثر التکاثر",
            "١٠٣ 103	alasr الـعصرالعصر",
            "١٠٤ 104	alhumazah  الھمزہ الـهمزة",
            "١٠٥ 105	alfil  alfeel  الفیل الـفيل",
            "١٠٦ 106	qurayish alquraysh quraysh alquraish  القریش قريش",
            "١٠٧ 107	almaun المـاعون الماعون",
            "١٠٨ 108	alkauthar alkauther alkawthar alkausr الـكوثر الکوثر",
            "١٠٩ 109	alkafirun alkafiroon alqafirun  الکافرون الـكافرون",
            "١١٠ 110	annasr  alnasr النصر الـنصر",
            "١١١ 111	almasad almasadd lahab tabbat allahab اللـھب اللھب اللہب",
            "١١٢ 112	alikhlas attauhid attawhid الاخلاص الاخلاص",
            "١١٣ 113	alfalaq  alfalak  الفلق الـفلق",
            "١١٤ 114	annas  alnas annaas الـناس الناس"};

}