package com.iroshni.urduquran15lines;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;


public class PageNumberList extends Fragment {
	TextView txtView_go_number;
	SeekBar seekbar;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
       final ViewGroup container, Bundle savedInstanceState) {
 		
        txtView_go_number = new TextView(inflater.getContext());
        txtView_go_number.setText(Html.fromHtml("<b>Page: " + 1 + "</b>" ));
        txtView_go_number.setGravity(Gravity.CENTER_HORIZONTAL);        	

        seekbar = new SeekBar(inflater.getContext());
        seekbar.setMax(AwesomePagerActivity.NUM_AWESOME_VIEWS-1);
        seekbar.setOnSeekBarChangeListener( new OnSeekBarChangeListener(){
       	 public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
       	 {
       	 	txtView_go_number.setText(Html.fromHtml("<b>Page: " + (progress+1) + "</b>" ));
       	 }
       	 public void onStartTrackingTouch(SeekBar seekBar){}
       	 public void onStopTrackingTouch(SeekBar seekBar){}
        });

        Button b1 = new Button(inflater.getContext());
        b1.setText("-");
        b1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				seekbar.setProgress(seekbar.getProgress()-1);
			}
		});
        
        Button b2 = new Button(inflater.getContext());
        b2.setText("+");
        b2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				seekbar.setProgress(seekbar.getProgress()+1);
			}
		});

        LinearLayout.LayoutParams layoutParams_seekbar = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams_seekbar.weight = 1;
        layoutParams_seekbar.gravity = Gravity.CENTER_VERTICAL;

        LinearLayout.LayoutParams layoutParams_slider = new LinearLayout.LayoutParams(
        		LinearLayout.LayoutParams.FILL_PARENT,
        		LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams_slider.setMargins(0, 0, 0, 30);
        
        LinearLayout ll = new LinearLayout(inflater.getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setGravity(Gravity.CENTER_VERTICAL);

        LinearLayout ll2 = new LinearLayout(inflater.getContext());
        ll2.addView(b1);
        ll2.addView(seekbar,layoutParams_seekbar);
        ll2.addView(b2);

        //LayoutInflater factory = LayoutInflater.from(this);
        final TableLayout go_cancel_button = (TableLayout) inflater.inflate(R.layout.page_number, null);
        go_cancel_button.setGravity(Gravity.CENTER_HORIZONTAL);
        
        ll.addView(txtView_go_number);
        ll.addView(ll2,layoutParams_slider);
        ll.addView(go_cancel_button);

        //setContentView(ll);
		AwesomePagerActivity.index_activity_response = -1;
		return ll;
    }
	
    @Override 
	public void onResume()
	{
		super.onResume();
		AwesomePagerActivity.index_activity_response = -1;
	}
        
}//class