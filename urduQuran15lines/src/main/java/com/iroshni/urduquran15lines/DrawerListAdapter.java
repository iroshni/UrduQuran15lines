package com.iroshni.urduquran15lines;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerListAdapter extends BaseAdapter {
	 
    Context mContext;
    ArrayList<NavItem> mNavItems;
    int offset=3;
 
    public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
        mContext = context;
        mNavItems = navItems;
    }
 
    @Override
    public int getCount() {
        return mNavItems.size()-offset;
    }
 
    @Override
    public Object getItem(int position) {
        return mNavItems.get(position+offset);
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
 
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.drawer_item, null);
        }
        else {
            view = convertView;
        }
 
        TextView titleView = (TextView) view.findViewById(R.id.title);
        TextView subtitleView = (TextView) view.findViewById(R.id.subTitle);
        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        
        int mod_position = position+offset;
 
        titleView.setText( mNavItems.get(mod_position).mTitle );
        subtitleView.setText( mNavItems.get(mod_position).mSubtitle );
        iconView.setImageResource(mNavItems.get(mod_position).mIcon);
        view.setId(mNavItems.get(mod_position).mId);
 
        return view;
    }
    
    public void enableFullMenu(boolean toggle)
    {
    	if((toggle && offset == 0) || (!toggle && offset == 3) )
    		return;
    	
    	offset = toggle?0:3;
    	notifyDataSetChanged();
    }
}


class NavItem {
    String mTitle;
    String mSubtitle;
    int mIcon;
    int mId;
 
    public NavItem(String title, String subtitle, int icon, int id) {
        mTitle = title;
        mSubtitle = subtitle;
        mIcon = icon;
        mId = id;
    }
}