package com.iroshni.urduquran15lines;

import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Switch;

public class SettingsActivity extends AppCompatActivity implements OnClickListener {
	
	Switch nightmodeButton, enableScreenTaps;;
	RadioButton port, land, auto, tapButtonScreen, tapButtonHightlight;
	RadioGroup radiogroup;
	CheckBox  enableFullScreen;
	private Switch volButton;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);        

        if( AwesomePagerActivity.appOrientation != AwesomePagerActivity.ORIENTATION_AUTO)
        	setRequestedOrientation(AwesomePagerActivity.appOrientation==AwesomePagerActivity.ORIENTATION_LANDSCAPE?ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        if(AwesomePagerActivity.appBrightnessValue > 0)
        {
        	WindowManager.LayoutParams layout_for_brightness = getWindow().getAttributes();
			layout_for_brightness.screenBrightness = AwesomePagerActivity.appBrightnessValue;
			getWindow().setAttributes(layout_for_brightness);
        }
        
        setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
        setTitle("Settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        
        enableScreenTaps = (Switch) findViewById(R.id.enable_screentaps_ckbox);
        enableScreenTaps.setChecked(AwesomePagerActivity.enableScreenTap);
        
		tapButtonScreen = (RadioButton) findViewById(R.id.screentap_btn);
		tapButtonScreen.setChecked(AwesomePagerActivity.tapGestureEnabled);
		tapButtonScreen.setText(Html.fromHtml("Tap to turn pages<br><small><i>Single tap to forward; Double tap to backward</i></small>"));
		tapButtonScreen.setEnabled(AwesomePagerActivity.enableScreenTap);
		ColorStateList textColors =  tapButtonScreen.getTextColors();
		
		enableFullScreen = (CheckBox) findViewById(R.id.fullscreen_btn);
		enableFullScreen.setText(Html.fromHtml("Show in portrait orientation<br><small><i>When hidden, swipe-down to show title bar / menu</i></small>"));
		enableFullScreen.setChecked(!AwesomePagerActivity.enableFullScreen);
		final CheckBox titleBar = (CheckBox) findViewById(R.id.checkBox_landscape_ab);	
		titleBar.setChecked(AwesomePagerActivity.showActionBarLS);
		
		volButton = (Switch) findViewById(R.id.vol_btn);
		volButton.setChecked(AwesomePagerActivity.volKeysEnabled);


		tapButtonHightlight = (RadioButton) findViewById(R.id.highlight_btn);
		tapButtonHightlight.setChecked(AwesomePagerActivity.highlightsEnabled);
		tapButtonHightlight.setText(Html.fromHtml("Tap to highlight text<br><small><i>Single tap to highlight, double tap to remove it</i></small>"));
		tapButtonHightlight.setEnabled(AwesomePagerActivity.enableScreenTap);

		radiogroup = (RadioGroup) findViewById(R.id.radioGroup2);
		LinearLayout mContainerView = (LinearLayout) findViewById(R.id.root_view);

		if(AwesomePagerActivity.svg == false)
		{
			mContainerView.removeView(findViewById(R.id.color_view));		
		}else
		{
			int i=AwesomePagerActivity.colorScheme;
			radiogroup.check(i==0?R.id.cscheme0:i==1?R.id.cscheme1:i==2?R.id.cscheme2:i==3?R.id.cscheme3:R.id.cscheme4);
		}
				
		Button zoomButtonLs = (Button) findViewById(R.id.zoom_btn_landscape);
		zoomButtonLs.setOnClickListener(this);
		zoomButtonLs.setTextColor(textColors);
		
		Button zoomButtonPt = (Button) findViewById(R.id.zoom_btn_portrait);
		zoomButtonPt.setOnClickListener(this);
		zoomButtonPt.setTextColor(textColors);
								
		port = (RadioButton) findViewById(R.id.radio_port);
		port.setChecked(AwesomePagerActivity.appOrientation==AwesomePagerActivity.ORIENTATION_PORTRAIT);
				
		auto = (RadioButton) findViewById(R.id.radio_auto);
		auto.setChecked(AwesomePagerActivity.appOrientation==AwesomePagerActivity.ORIENTATION_AUTO);
				
		land = (RadioButton) findViewById(R.id.radio_land);
		land.setChecked(AwesomePagerActivity.appOrientation==AwesomePagerActivity.ORIENTATION_LANDSCAPE);
        
		RadioGroup radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);        
	    radioGroup1.setOnCheckedChangeListener(new OnCheckedChangeListener() 
	    {
	        public void onCheckedChanged(RadioGroup group, int checkedId) {
	        	AwesomePagerActivity.appOrientation = port.isChecked()?AwesomePagerActivity.ORIENTATION_PORTRAIT:(land.isChecked()?AwesomePagerActivity.ORIENTATION_LANDSCAPE:AwesomePagerActivity.ORIENTATION_AUTO);
	        	AwesomePagerActivity.index_activity_response = 0xABC03;
	        }
	    });		
            
	    CompoundButton.OnCheckedChangeListener	listener =	new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton  buttonView,boolean isChecked) {
            	
            	AwesomePagerActivity.enableFullScreen = !enableFullScreen.isChecked();
            	AwesomePagerActivity.showActionBarLS = titleBar.isChecked();            		
                AwesomePagerActivity.index_activity_response = 0xABC03;
                
            }
         };
         
         enableFullScreen.setOnCheckedChangeListener(listener);
         
		RadioGroup radioGroup21 = (RadioGroup) findViewById(R.id.radioGroup21);        
	    radioGroup21.setOnCheckedChangeListener(new OnCheckedChangeListener() 
	    {
	        public void onCheckedChanged(RadioGroup group, int checkedId) {
                if( checkedId == R.id.screentap_btn)
                {
                	AwesomePagerActivity.tapGestureEnabled = true;
                	AwesomePagerActivity.highlightsEnabled = false;
                }
                else if( checkedId == R.id.highlight_btn)
                {
                	AwesomePagerActivity.tapGestureEnabled = false;
                	AwesomePagerActivity.highlightsEnabled = true;
                }
                
                AwesomePagerActivity.index_activity_response = 0xABC03;
            }
         });			
     
        volButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton  buttonView,boolean isChecked) {
                AwesomePagerActivity.volKeysEnabled = isChecked;
                AwesomePagerActivity.index_activity_response = 0xABC03;
                
            }
         });			

        
        enableScreenTaps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton  buttonView,boolean isChecked) {
            	AwesomePagerActivity.enableScreenTap = isChecked;
            	tapButtonHightlight.setEnabled(isChecked);
            	tapButtonScreen.setEnabled(isChecked);
                AwesomePagerActivity.index_activity_response = 0xABC03;
                
            }
         });			

        titleBar.setOnCheckedChangeListener(listener);			
        
        radiogroup.setOnCheckedChangeListener(new OnCheckedChangeListener() 
	    {
	        public void onCheckedChanged(RadioGroup group, int checkedId) {
	    		int i = radiogroup.getCheckedRadioButtonId();
	   			AwesomePagerActivity.colorScheme = i==R.id.cscheme0?0:i==R.id.cscheme1?1:i==R.id.cscheme2?2:i==R.id.cscheme3?3:4;
	        	AwesomePagerActivity.index_activity_response = 0xABC03;
	        }
	    });		
        

		AwesomePagerActivity.index_activity_response = -1;        
    }

    public void onClick(View v) {
    	
    	switch (v.getId())
    	{
    	case R.id.zoom_btn_landscape:
    		AwesomePagerActivity.index_activity_response = 0xABC00;
    		break;    		
    	case R.id.zoom_btn_portrait:
    		AwesomePagerActivity.index_activity_response = 0xABC01;
    		break;
    	}    	
        finish();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    
}

