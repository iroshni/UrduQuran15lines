package com.iroshni.urduquran15lines;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Picture;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.google.android.vending.expansion.zipfile.ZipResourceFile;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AwesomePagerActivity extends AppCompatActivity{
    
	CustomViewPager awesomePager;
	ThreeElementStorage webViewReference;
	private Context cxt;
	MyWebView webView;
	private AwesomePagerAdapter awesomeAdapter;
	int displayHeight, displayWidth;
	Editor preferenceEditor;
	EditText ev;
	AlertDialog.Builder zoomOptionDialog;
	int zoom_option[], zoom_option_req, orientation;
	boolean zoom_controls[];
	static boolean databaseExist =false;
	static File SDCardRoot = Environment.getExternalStorageDirectory();
	static int index_activity_response;
	static final int FIT_WIDTH=0, FILL_SCREEN=1, NO_ZOOM=2;
	static final int ORIENTATION_LANDSCAPE=0, ORIENTATION_PORTRAIT=1,ORIENTATION_AUTO=2; 
	static final int DIALOG_SET_BOOKMARK=0, DIALOG_PROGRESS_BAR=1, DIALOG_ZOOM_OPTION_LANDSCAPE=3, DIALOG_ZOOM_OPTION_PORTRAIT=4, DIALOG_GO=5, DIALOG_SET_BRIGHTNESS=6, DATA_FILES_SVG_DOWNLOAD_DIALOG=7, DIALOG_SHARE=8, DIALOG_SAVE=9;
	static final int DRAWER_ITEM_GOTO=0, DRAWER_ITEM_BRIGHTNESS=1, DRAWER_ITEM_SHARE=2,DRAWER_ITEM_SAVE=3, DRAWER_ITEM_SETTINGS=4, DRAWER_ITEM_ABOUT=5, DRAWER_ITEM_SET_BOOKMARK=6, DRAWER_ITEM_BOOKMARK_LIST=7, DRAWER_ITEM_INDEX=8, DRAWER_ITEM_LOCAL_BACKUP=10, DRAWER_ITEM_LOCAL_RESTORE=11; 
	static final int NUM_AWESOME_VIEWS = 611;
	static final boolean skipDatabaseCheck = false; //Build.FINGERPRINT.startsWith("generic");
	static int colorScheme;
	final String colors[][] = {{"#999999","#000000"},{"#000000","#828282"},{"#000000","#008000"},{"#002A35","#8C8CA0"}, {"#FBF0D9","#000000"}};
    static boolean tapGestureEnabled=false;
    static int appOrientation=ORIENTATION_AUTO;	
	TextView txtView_go_number, txtView_go_surahName;
	SeekBar seekbar;
	RadioGroup goButtonGroup, pZoomButtonGroup, lZoomButtonGroup;
	CheckBox zoomCtr_chkBox_pt, zoomCtr_chkBox_ls, autoBrightness_chkBox;
	SeekBar seekbar_brightness;
	boolean auto_brightness;
	float sysBrightnessValue; 
	static float appBrightnessValue ;	
	WindowManager.LayoutParams layout_for_brightness; 
	static boolean svg=false;
	WebView liveWebView[];
    final String appPath = "/UrduQuran15lines/";
    int sharedPages=0;
    static boolean showActionBarLS=false;
    android.support.v7.app.ActionBar aBar;
	String savedPageFilePath=null;
    static boolean hardwareMenuKeyPresent=false;
    ProgressBar readingProgressbar;
    ZipResourceFile zrf, zrf_ext;   
    float svgScale[][];
    static List<LinkedList<HighlightRect>> highligts=new ArrayList<LinkedList<HighlightRect>>(NUM_AWESOME_VIEWS+1);
    static boolean enableScreenTap;
    String SVGStrCache_header[], SVGStrCache_body[] ;
	static boolean volKeysEnabled;
    static boolean highlightsEnabled=false;
    static boolean saveSettings = false;
    static List<BookmarkObject> bookmarks = new ArrayList<BookmarkObject>();
    boolean waitForLayout;
	static boolean enableFullScreen;
	private Handler handlerActionBar = new Handler();
	Runnable runnableActionBar;
    private ListView mDrawerList;
    private DrawerListAdapter mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    BackupManager backupManager;
    SharedPreferences preferences;
    
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        getOrientation();
    	preferences = getSharedPreferences("AwesomePagerActivity_prefs", Context.MODE_PRIVATE);
    	preferenceEditor  = preferences.edit();
    	resolveBackwardPreferenceCompatibilty();
        backupManager = new BackupManager(this);        
        
        showActionBarLS = preferences.getBoolean("showActionBarLS", this.isLargeDisplay(this));
        appOrientation = preferences.getInt("appOrientation", ORIENTATION_AUTO);
        if( appOrientation != ORIENTATION_AUTO)
        	setRequestedOrientation(appOrientation==ORIENTATION_LANDSCAPE?ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        enableFullScreen = preferences.getBoolean("enableFullScreen", true);
        
        if(orientation == ORIENTATION_PORTRAIT && enableFullScreen)
        	setContentView(R.layout.main_toolbar_overlay);
        else
        	setContentView(R.layout.main_toolbar_no_overlay);
        
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

       setSupportActionBar(mToolbar);

		aBar = this.getSupportActionBar();//getActionBar();
		aBar.setTitle("Quran");
	    if(orientation == ORIENTATION_LANDSCAPE)
	    {
	        	if(showActionBarLS == true)
	        		aBar.show();
	        	else
	        	{
	        		if(appOrientation != ORIENTATION_LANDSCAPE)
	        			aBar.hide();
	        		else
	        			aBar.show();
	        	}
	    }
        
		mDrawerList = (ListView)findViewById(R.id.navList);mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true); 
        
        
        // Possible work around for market launches. See http://code.google.com/p/android/issues/detail?id=2373
        // for more details. Essentially, the market launches the main activity on top of other activities.
        // we never want this to happen. Instead, we check if we are the root and if not, we finish.
         if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction(); 
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                //Log.w(LOG_TAG, "Main Activity is not the root.  Finishing Main Activity instead of launching.");
           	 //Toast.makeText(getApplicationContext(), "Main Activity is not the root.  Finishing Main Activity instead of launching.", Toast.LENGTH_SHORT).show();
                finish();
                return;       
            }
          }

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		    databaseExist = doesXApkFilesExist(); //preferences.getBoolean("APKFileExist", false);
        
        int zoom_option_landscape  = preferences.getInt("zoom_option_landscape", FIT_WIDTH);
        int zoom_option_portrait   = preferences.getInt("zoom_option_portrait", FILL_SCREEN);
        zoom_option = new int[]{zoom_option_landscape,zoom_option_portrait };

        boolean zoom_controls_landscape  = preferences.getBoolean("zoom_controls_landscape", false);
        boolean zoom_controls_portrait   = preferences.getBoolean("zoom_controls_portrait", false);
        zoom_controls = new boolean[]{zoom_controls_landscape,zoom_controls_portrait };
        
        svg = true; 

        colorScheme = preferences.getInt("colorScheme", 4);
        tapGestureEnabled = preferences.getBoolean("tapGestureEnabled", true);
        highlightsEnabled = preferences.getBoolean("highlightsEnabled", false);
        enableScreenTap = preferences.getBoolean("enableScreenTap", true);
        volKeysEnabled = preferences.getBoolean("volKeysEnabled", true);

        //Avoid screen dim
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); 
   		layout_for_brightness = getWindow().getAttributes();
   		appBrightnessValue  = preferences.getFloat("appBrightnessValue", -1.0f);
   		try
   		{
   			int mode = Settings.System.getInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
   			if (mode != Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC)
   				sysBrightnessValue = android.provider.Settings.System.getInt(getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS)/255.0f;   			
   			if(sysBrightnessValue == 0 )
   				sysBrightnessValue = -0.1f;
   			else
   			{
   				sysBrightnessValue = -1.0f;
   			}
   		}catch(Exception ex)
   		{
   			sysBrightnessValue = -1.0f;
   		}

   		if(appBrightnessValue > 0)
		{
			layout_for_brightness.screenBrightness = appBrightnessValue;
			getWindow().setAttributes(layout_for_brightness);
		}
   		   		
        cxt = this;
        liveWebView = new WebView[3];
        svgScale    = new float[3][2];
        SVGStrCache_header = new String[3];
        SVGStrCache_body = new String[3];
        loadHighlights();
        loadBookmarks();

        readingProgressbar = (ProgressBar) findViewById(R.id.reading_progressbar);
       	readingProgressbar.setRotation(180);
		
        webViewReference = new ThreeElementStorage();
        awesomeAdapter = new AwesomePagerAdapter(this); 
        awesomePager = (CustomViewPager) findViewById(R.id.awesomepager);
        awesomePager.setAdapter(awesomeAdapter);
        awesomePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

        	@Override 
        	public void onPageSelected(int position) 
        	{ 
        		//awesomePager.setCurrentPageWebView(mLstPagesWebviews_.get(position)); 
        		awesomePager.setCurrentPageWebView(webViewReference.get(position));
        	}

        	@Override
        	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        	@Override
        	public void onPageScrollStateChanged(int state) {}
        });
		
        waitForLayout = true;
        ViewTreeObserver vto = awesomePager.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
    				
           @Override
           @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
           public void onGlobalLayout() {		    
               //remove listener to ensure only one call is made.
               if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) 
            	   awesomePager.getViewTreeObserver().removeGlobalOnLayoutListener(this);
               else
            	   awesomePager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                       	   
	    	   displayHeight = awesomePager.getHeight();
	    	   displayWidth = awesomePager.getWidth();
	    	   waitForLayout = false;
	    	   
	    	   //Toast.makeText(AwesomePagerActivity.this, "--> "+displayWidth+"x"+displayHeight, Toast.LENGTH_SHORT).show();

	   		if(skipDatabaseCheck == true)
				setCurrentPage(1);
			else{
				if(databaseExist == false)			
					AwesomePagerActivity.this.startActivityForResult(new Intent().setClass(AwesomePagerActivity.this, APKDownloaderActivity.class),preferences.getInt("currentPage", 0));		
				else
				{
				  try{
					  String ApkFileName = Helpers.getExpansionAPKFileName(AwesomePagerActivity.this, APKDownloaderActivity.xAPKS[0].mIsMain, APKDownloaderActivity.xAPKS[0].mFileVersion);
					  zrf = new ZipResourceFile(Helpers.generateSaveFileName(AwesomePagerActivity.this, ApkFileName));
					  
					  ApkFileName = Helpers.getExpansionAPKFileName(AwesomePagerActivity.this, APKDownloaderActivity.xAPKS[1].mIsMain, APKDownloaderActivity.xAPKS[1].mFileVersion);
					  zrf_ext = new ZipResourceFile(Helpers.generateSaveFileName(AwesomePagerActivity.this, ApkFileName));
					  				  
				  }catch(Exception e){e.printStackTrace();}
				  setCurrentPage(preferences.getInt("currentPage", 1));
				  awesomePager.getAdapter().notifyDataSetChanged();
				}
			 }
	    	   
           }
            
        });

		
        runnableActionBar = new Runnable() {
       	   @Override
       	   public void run() {
       		   if(enableFullScreen && orientation == ORIENTATION_PORTRAIT)
       			   aBar.hide();
       	   }
       	};
  					
       autoHideActionBar();
       if (databaseExist == true && preferences.getBoolean("ShowTutorial", true))    
       {
     	  startActivity(new Intent().setClass(AwesomePagerActivity.this, WalkthroughActivity.class));
     	  preferenceEditor.putBoolean("ShowTutorial", false);
     	  preferenceEditor.commit();

       }
	

    if( getIntent().getExtras() != null && getIntent().getExtras().getString("code") != null)
      Toast.makeText(this, "New Settings Applied", Toast.LENGTH_LONG).show();
    }

  private boolean doesXApkFilesExist()
  {
    for( APKDownloaderActivity.XAPKFile xf: APKDownloaderActivity.xAPKS)
    {
      String ApkFileName = Helpers.getExpansionAPKFileName(AwesomePagerActivity.this, xf.mIsMain, xf.mFileVersion);
      if( !Helpers.doesFileExist(AwesomePagerActivity.this, ApkFileName, xf.mFileSize, false))
        return false;
    }
    return true;
  }
       
    private void addDrawerItems() {
	    mNavItems.add(new NavItem("Set bookmark", "Bookmark visible page", R.drawable.ic_menu_star, DRAWER_ITEM_SET_BOOKMARK));
	    mNavItems.add(new NavItem("Bookmarks", "List of your bookmarks", R.drawable.bookmarks, DRAWER_ITEM_BOOKMARK_LIST));
	    mNavItems.add(new NavItem("Index", "Indices of Surahs & Parahs", R.drawable.ic_index, DRAWER_ITEM_INDEX));
    	mNavItems.add(new NavItem("Go To", "Jump to an Ayat or Ruku", R.drawable.ic_menu_goto, DRAWER_ITEM_GOTO));
    	mNavItems.add(new NavItem("Brightness", "Change screen brightness", R.drawable.ic_brightness, DRAWER_ITEM_BRIGHTNESS));
    	mNavItems.add(new NavItem("Share", "Share visible page", R.drawable.ic_menu_share, DRAWER_ITEM_SHARE));
    	mNavItems.add(new NavItem("Save", "Save visible page", R.drawable.ic_menu_save, DRAWER_ITEM_SAVE));
    	mNavItems.add(new NavItem("Settings", "Customize your app", R.drawable.ic_menu_preferences, DRAWER_ITEM_SETTINGS));
          mNavItems.add(new NavItem("Backup", "Backup settings/bookmarks", R.drawable.backup, DRAWER_ITEM_LOCAL_BACKUP));
    mNavItems.add(new NavItem("Restore", "Restore settings/bookmarks", R.drawable.restore, DRAWER_ITEM_LOCAL_RESTORE));
    	mNavItems.add(new NavItem("About", "Get to know about us", R.drawable.ic_menu_info_details, DRAWER_ITEM_ABOUT));
    	
    	mAdapter = new DrawerListAdapter(this, mNavItems);
    	mAdapter.enableFullMenu(true);
    	
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	
            	int vId = view.getId();
            	
        		switch (vId) {
    			case AwesomePagerActivity.DRAWER_ITEM_GOTO:
    				//AwesomePagerActivity.this.finish();
    				if(txtView_go_surahName != null)
    				{

    					txtView_go_surahName.setText("Surah "+getSurahName(getCurrentPage()).split(":")[1]);         
    					seekbar.setProgress(0);
    					
    					if(goButtonGroup.getCheckedRadioButtonId() == R.id.id_ayat) //Ayat radio button
    						seekbar.setMax(totalAyaat[getCurrentSurahIndex()]-1);
    					else
    						seekbar.setMax(totalRuku[getCurrentSurahIndex()]-1);
    				}

    				showDialog(DIALOG_GO);
    				break;
    			
    			case AwesomePagerActivity.DRAWER_ITEM_BRIGHTNESS:
    	    		if( seekbar_brightness != null)
    	    		{
    	    			seekbar_brightness.setProgress((int)((appBrightnessValue<=0? sysBrightnessValue :appBrightnessValue)*10)-1);
    	    			autoBrightness_chkBox.setChecked(appBrightnessValue<=0);
    	    		}

    	    		showDialog(DIALOG_SET_BRIGHTNESS);				
    				break;
    	            
    	        case AwesomePagerActivity.DRAWER_ITEM_SHARE:				    					
    				AwesomePagerActivity.this.showDialog(DIALOG_SHARE);
    				break;
    	        case AwesomePagerActivity.DRAWER_ITEM_SAVE:				
    	        	prepareSavedFile();
    	        	MediaScannerConnection.scanFile(AwesomePagerActivity.this,
    	                    new String[] { savedPageFilePath }, null,
    	                    new MediaScannerConnection.OnScanCompletedListener() {
    	                public void onScanCompleted(String path, Uri uri) {		        		
    	                }
    	            });

    	        	showDialog(DIALOG_SAVE);
    				break;
    							
          case DRAWER_ITEM_LOCAL_BACKUP:
            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);

            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("application/octet-stream"); //json mime type
            final PackageManager pm = getApplicationContext().getPackageManager();
            ApplicationInfo ai=null;
            try {
              ai = pm.getApplicationInfo( getPackageName(), 0);
            } catch (final NameNotFoundException e) {}
            final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "settings");
            intent.putExtra(Intent.EXTRA_TITLE, applicationName.replaceAll(" ","_").replaceAll("/","_")+".json");

            startActivityForResult(intent, 0x123F);

            break;

          case DRAWER_ITEM_LOCAL_RESTORE:
            Intent intent1 = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent1.addCategory(Intent.CATEGORY_OPENABLE);
            intent1.setType("application/octet-stream");

            startActivityForResult(intent1, 0x123E);

            break;

    			case AwesomePagerActivity.DRAWER_ITEM_ABOUT:
    				String versionName=null;
    				try {
    					versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
    				} catch (NameNotFoundException e) {}
    				final String info = "<p>"+versionName==null?"":("version "+versionName+"<br />")+
    						  "<a href=\"mailto:iroshni@outlook.com\">iRoshni@outlook.com</a></p>";
    				
    	        	Spanned msg = Html.fromHtml(info);
    		   		AlertDialog alertDialog = new AlertDialog.Builder(AwesomePagerActivity.this).create();
    			    alertDialog.setTitle("About");			
    			    alertDialog.setMessage(msg);			
    			    alertDialog.setIcon(R.drawable.ic_menu_info_details);		
    			    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
    			            public void onClick(DialogInterface dialog, int which) {
    			            }
    			    });
    			    alertDialog.show();		
    			    ((TextView)alertDialog.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    			    break;

    			case AwesomePagerActivity.DRAWER_ITEM_SETTINGS:
    	            AwesomePagerActivity.this.startActivityForResult(new Intent().setClass(AwesomePagerActivity.this, SettingsActivity.class),0);
    	            break;
    	            
    			case AwesomePagerActivity.DRAWER_ITEM_SET_BOOKMARK:
    				showBookmarkAdd();
    				break;

    			case AwesomePagerActivity.DRAWER_ITEM_BOOKMARK_LIST:
    				showBookmarkList();
    				break;

    			case AwesomePagerActivity.DRAWER_ITEM_INDEX:
    				showIndex();
    				break;

        		}
        	    // Close the drawer
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);                
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                mAdapter.enableFullMenu(true);
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }    
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    public void resolveBackwardPreferenceCompatibilty()
    {
    	if( !getPreferences(Context.MODE_PRIVATE).getAll().isEmpty()) 
    	{
    		SharedPreferences sp = getPreferences(Context.MODE_PRIVATE);
    		for(Entry<String,?> entry : sp.getAll().entrySet()){ 
    			 Object v = entry.getValue(); 
    			 String key = entry.getKey();
    			 if(v instanceof Boolean) 
    				 preferenceEditor.putBoolean(key, ((Boolean)v).booleanValue());
    			 else if(v instanceof Float)
    				 preferenceEditor.putFloat(key, ((Float)v).floatValue());
    			 else if(v instanceof Integer)
    				 preferenceEditor.putInt(key, ((Integer)v).intValue());
    			 else if(v instanceof Long)
    				 preferenceEditor.putLong(key, ((Long)v).longValue());
    			 else if(v instanceof String)
    				 preferenceEditor.putString(key, ((String)v));         
    			}
    			preferenceEditor.commit(); //save it.
    			Editor e = sp.edit();
    			e.clear();
    			e.commit();
    	}
    }    
    
    @Override
    public void onResume() {
    	super.onResume();
        //wakelock.acquire();
    	//Log.d("OnResume", "OOOOOOOOOOOO");
    }

    public void autoHideActionBar()
    {
    	handlerActionBar.removeCallbacks(runnableActionBar);
    	handlerActionBar.postDelayed(runnableActionBar, 4000);
    }

    private void loadBookmarks()
    {
		bookmarks.clear();
		if(preferences.contains("bookmarks_desc"))
		{
			String[] tokens_bookmark_string =  preferences.getString("bookmarks_desc", "").split("\n");
			String[] tokens_bookmark_pages  =  preferences.getString("bookmarks_pages", "").split("\n");
			
			try
			{
				for(int i=0; i<tokens_bookmark_string.length; i++)
				{
					if(tokens_bookmark_string[i].length() == 0 )
						continue;
				
					try{
						bookmarks.add(new BookmarkObject(tokens_bookmark_string[i], Integer.valueOf(tokens_bookmark_pages[i]), false));
					}catch(NumberFormatException ex){}
				}
				
			}catch(java.lang.ArrayIndexOutOfBoundsException ex)
			{
				bookmarks.clear();
			}
			
			preferenceEditor.remove("bookmarks_desc");
			preferenceEditor.remove("bookmarks_pages");

		}else if(preferences.contains("json_bookmarks_desc"))
		{
			try {
				JSONArray desc = new JSONArray(preferences.getString("json_bookmarks_desc", "[]"));
				JSONArray pages = new JSONArray(preferences.getString("json_bookmarks_pages", "[]"));
				String s;
				
				for (int i = 0; i < desc.length(); i++) {
					s = desc.getString(i);
					
					if(s.length()==0) 
						continue;
					
					bookmarks.add(new BookmarkObject(s, (Integer)pages.get(i), false ));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			preferenceEditor.remove("json_bookmarks_desc");
			preferenceEditor.remove("json_bookmarks_pages");
			
		}else 
		{
			String t = preferences.getString("bookmarks", null);
			if( t != null)
				bookmarks = new Gson().fromJson(t, new TypeToken<List<BookmarkObject>>(){}.getType());			
		}
    	
    }

    private class AwesomePagerAdapter extends PagerAdapter{
	    final String mimeType = "text/html";// "image/svg+xml";
	    final String encoding = "utf-8";
	    String html = " ";
	    int htmlImageWidth, htmlImageHeight;
	    
	    AwesomePagerActivity awesomePagerActivity;
	        
	    public AwesomePagerAdapter(AwesomePagerActivity awesomePagerActivity) {
	            this.awesomePagerActivity = awesomePagerActivity; 
	    }
	 	    
	    @Override
		public int getCount() {
			return NUM_AWESOME_VIEWS;
		}
		
	    /**
	     * Create the page for the given position.  The adapter is responsible
	     * for adding the view to the container given here, although it only
	     * must ensure this is done by the time it returns from
	     * {@link #finishUpdate()}.
	     *
	     * @param container The containing View in which the page will be shown.
	     * @param position The page position to be instantiated.
	     * @return Returns an Object representing the new page.  This does not
	     * need to be a View, but can be some other container of the page.
	     */
		@Override
		public Object instantiateItem(View collection, int position) {
			int p = NUM_AWESOME_VIEWS-position;
		    
			RelativeLayout layout = new RelativeLayout(cxt);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.CENTER_IN_PARENT);		
			
			//if(databaseExist == true || skipDatabaseCheck == true)
			{

				webView = new MyWebView(cxt, awesomePagerActivity);
				webView.getSettings().setSupportZoom(true);  
				webView.setVerticalScrollBarEnabled(true);
				webView.setHorizontalScrollBarEnabled(true);
				webView.setBackgroundColor(Color.WHITE);
				webView.setInitialScale(99);
				webViewReference.put(webView, position);
				webView.showZoomButtons(zoom_controls[orientation]);
				webView.getSettings().setBuiltInZoomControls(true);
				//webView.getSettings().setJavaScriptEnabled(true);
//				Log.d("instantiateItem","Zoom: "+zoom_controls[orientation]);	
				liveWebView[p%3] = webView;
				
				if(position == awesomePager.getCurrentItem())     //Sometimes on page listner is called before this method 
					awesomePager.setCurrentPageWebView(webView);
				
				System.gc();

				//html = "<html><head> <meta name=\"viewport\" content=\"target-densitydpi=device-dpi,  user-scalable=yes\" /> </head>";
				
				if(waitForLayout == false && isStoragePermissionGranted())
				{

					if(zoom_option[orientation] == FIT_WIDTH || (highlightsEnabled && enableScreenTap))
					{
						htmlImageWidth  = displayWidth;
		                html = creatHtmlPage(p, htmlImageWidth, -1);
		                webView.setBackgroundColor(Color.parseColor(colors[colorScheme][0]));
					}
					else //if(zoom_option[orientation] == FILL_SCREEN)
					{
						htmlImageWidth  = displayWidth;
						htmlImageHeight =  displayHeight;
		                html = creatHtmlPage(p, htmlImageWidth, htmlImageHeight);
		                webView.setBackgroundColor(Color.parseColor(colors[colorScheme][0]));
					}
				}

				//html += "<img src="+"file:///"+SDCardRoot.getPath()+"/test/test.png"+" /> </html>";
				//webView.loadDataWithBaseURL(null, html, mimeType, encoding, "");
				webView.loadDataWithBaseURL(null, html, mimeType, encoding,"");
				setProgressParah();
				//html = "file:///"+SDCardRoot.getPath()+"/test/test.png"; webView.loadData(html,"text/html",encoding);
				
				//Log.d("AwesomePageActivity",html+" "+position);
				//Log.d("AwesomePageActivity","file: "+SDCardRoot.getPath()+appPath+(prefix+(p))+".gif -> "+new File(SDCardRoot.getPath()+appPath+(prefix+(p))+".gif").exists());
				//Log.d("AwesomePageActivity","current item: "+awesomePager.getCurrentItem());
							    
			}
			//AwesomePagerActivity.this.registerForContextMenu(webView);


			
			layout.addView(webView, params);
			((ViewPager) collection).addView(layout,0);
			return layout;
			
		}

		
		
		
		@Override
		public int getItemPosition(Object object) {
		    return POSITION_NONE;
		}

	    /**
	     * Remove a page for the given position.  The adapter is responsible
	     * for removing the view from its container, although it only must ensure
	     * this is done by the time it returns from {@link #finishUpdate()}.
	     *
	     * @param container The containing View from which the page will be removed.
	     * @param position The page position to be removed.
	     * @param object The same object that was returned by
	     * {@link #instantiateItem(View, int)}.
	     */
		@Override
		public void destroyItem(View collection, int position, Object view) {
			//((ViewPager) collection).removeView((TextView) view);
			((ViewPager) collection).removeView((View) view);
			System.gc();
		//Log.d("destroy Item", "Garbage Collected");
		}

		
		
		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view==object;
			//return ( view==((ScrollView)object) || view==((LinearLayout)object)) ;
		}

		
	    /**
	     * Called when the a change in the shown pages has been completed.  At this
	     * point you must ensure that all of the pages have actually been added or
	     * removed from the container as appropriate.
	     * @param container The containing View which is displaying this adapter's
	     * page views.
	     */
		@Override
		public void finishUpdate(View arg0) {}
		

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {}
    	
    }
    
    boolean asked = false;
    @SuppressLint("NewApi")
	public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } 
            else {

            	if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            	  {
            	    //Explain to the user why we need to read the contacts
            	    Snackbar.make(findViewById(R.id.RootView), "Storage access is required to read app data file.", Snackbar.LENGTH_INDEFINITE)
            	            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                	ActivityCompat.requestPermissions(AwesomePagerActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                	asked = true;
                                }
                            })
            	            .show();
            	  }
            	else{
	                if(!asked)
	                {
	                	ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
	                	asked = true;
	                }
            	}
            	
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation            
            return true;
        }
    }
    
    @SuppressLint("NewApi")
	@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        
    	super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        
    	//resume tasks needing this permission
    	if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
        	try {
				  String ApkFileName = Helpers.getExpansionAPKFileName(AwesomePagerActivity.this, APKDownloaderActivity.xAPKS[0].mIsMain, APKDownloaderActivity.xAPKS[0].mFileVersion);
				  zrf = new ZipResourceFile(Helpers.generateSaveFileName(AwesomePagerActivity.this, ApkFileName));			  
				  ApkFileName = Helpers.getExpansionAPKFileName(AwesomePagerActivity.this, APKDownloaderActivity.xAPKS[1].mIsMain, APKDownloaderActivity.xAPKS[1].mFileVersion);
				  zrf_ext = new ZipResourceFile(Helpers.generateSaveFileName(AwesomePagerActivity.this, ApkFileName));
				} catch (IOException e) {}
       
        	awesomePager.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    protected void onDestroy() {
    super.onDestroy();

    unbindDrawables(findViewById(R.id.RootView));
    System.gc();
    }

    private void unbindDrawables(View view) {
        
    	if (view.getBackground() != null) {
        	view.getBackground().setCallback(null);
        }
        
    	if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) 
            	unbindDrawables(((ViewGroup) view).getChildAt(i));
            
            if( !(view instanceof AdapterView) )
            	((ViewGroup) view).removeAllViews();
        }
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
        return true;	
	}
    
    private void processSettings(int code)
    {
    	boolean pageLayoutUpdateRequired = false;
    	
    	switch(code)
    	{
    	case 0xABC00:
	    	zoom_option_req  = zoom_option[ORIENTATION_LANDSCAPE]; //default value
			showDialog(DIALOG_ZOOM_OPTION_LANDSCAPE);
    		break;
		case 0xABC01:
	    	zoom_option_req  = zoom_option[ORIENTATION_PORTRAIT]; //default value
			showDialog(DIALOG_ZOOM_OPTION_PORTRAIT);
			break;
    	}
		    
		if(colorScheme != preferences.getInt("colorScheme", 1))
		{
			pageLayoutUpdateRequired = true;
			preferenceEditor.putInt("colorScheme", colorScheme);
		}
			        
		if(highlightsEnabled != preferences.getBoolean("highlightsEnabled", false))
		{
			pageLayoutUpdateRequired = true;
			preferenceEditor.putBoolean("highlightsEnabled", highlightsEnabled);
		}
		
		if(enableScreenTap != preferences.getBoolean("enableScreenTap", true))
		{
			preferenceEditor.putBoolean("enableScreenTap", enableScreenTap);
			if(highlightsEnabled)
				pageLayoutUpdateRequired=true;
			
		}
		
		if(volKeysEnabled != preferences.getBoolean("volKeysEnabled", true))
		{
			preferenceEditor.putBoolean("volKeysEnabled", enableScreenTap);
		}
			        
		if (pageLayoutUpdateRequired)
			awesomePager.getAdapter().notifyDataSetChanged();

		if( appOrientation != preferences.getInt("appOrientation", ORIENTATION_AUTO))
	        {
	        	if(appOrientation != ORIENTATION_AUTO)
	        		setRequestedOrientation(appOrientation==ORIENTATION_LANDSCAPE?ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	        	else
	        		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	        	
        		if(appOrientation == ORIENTATION_LANDSCAPE && hardwareMenuKeyPresent == false)
        			aBar.show();

        		preferenceEditor.putInt("appOrientation", appOrientation);
	        }

	        if( orientation == ORIENTATION_LANDSCAPE && aBar.isShowing() != showActionBarLS)
	        {
        		if(appOrientation == ORIENTATION_LANDSCAPE)
        		{
        			if (hardwareMenuKeyPresent == false) 
        				aBar.show();
        		}
        		else
        		{
        			if(showActionBarLS == true)
        			{
        				if (hardwareMenuKeyPresent == false) 
        					aBar.show();
        			}
	        		else
	        			aBar.hide();
	        	}
	        }
			
			preferenceEditor.putBoolean("tapGestureEnabled", tapGestureEnabled);
			preferenceEditor.putBoolean("showActionBarLS", showActionBarLS);
			preferenceEditor.commit();
			
			if( enableFullScreen != preferences.getBoolean("enableFullScreen", true))
			{
				preferenceEditor.putBoolean("enableFullScreen", enableFullScreen);
				preferenceEditor.commit();
        doSelfRestart();
      }

    }

  private void doSelfRestart()
  {
    Intent mStartActivity = new Intent(this, AwesomePagerActivity.class);
    mStartActivity.putExtra("code", "selfRestart");
    int mPendingIntentId = 125606;
    PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
    AlarmManager mgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
    finish();
    System.exit(0);
  }
   
    public void saveHighlights()
    {
		JSONArray ha = new JSONArray();		
		
		for( int i=0; i < highligts.size(); i++)
		{
			String s = "";
			LinkedList<HighlightRect> l = highligts.get(i);
			
			if( l != null)
				for(int j=0; j<highligts.get(i).size(); j++)
					s += l.get(j).toString()+":";

			if(s.length()> 0)
				s = s.substring(0,s.length()-1);

			ha.put(s);
		}
		
		preferenceEditor.putString("highligts", ha.toString());	
		preferenceEditor.commit();
    }
    
    public void loadHighlights()
    {
		try {
			
			//Initialization
			highligts.clear();
			
			if(preferences.contains("highligts") == false)
			{
				for(int i=0; i<=NUM_AWESOME_VIEWS;i++)
					highligts.add(null);

				return;
			}

			JSONArray ha = new JSONArray(preferences.getString("highligts", "[]"));
			
			for(int i=0; i< ha.length(); i++)
			{
				
				if(ha.getString(i)==null || ha.getString(i).isEmpty())
				{
					highligts.add(null);
				}
				else
				{
					String a[] = ha.getString(i).split(":");
					LinkedList<HighlightRect> l = new LinkedList<HighlightRect>();
					
					for(int j=0; j<a.length; j++)
						l.add(HighlightRect.fromString(a[j]));
					
					highligts.add(l);
				}
			}
			//PATCH: highligts array size must be equal to  NUM_AWESOME_VIEWS+1
			if(highligts.size() <= NUM_AWESOME_VIEWS)
				highligts.add(null);
		
		} catch (JSONException e) {
			
		}
		
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
        	mAdapter.enableFullMenu(false);
          return true;
        }

		switch (item.getItemId()) {
			case R.id.index:
				showIndex();
				return true;
				
			case R.id.bookmarks_list:
				showBookmarkList();
	            return true;

			case R.id.bookmarks_add:
				showBookmarkAdd();
				return true;

		}
		return super.onOptionsItemSelected(item);
	}
    
    private void showIndex()
    {
		AwesomePagerActivity.this.startActivityForResult(new Intent().setClass(AwesomePagerActivity.this, TabActivity.class),0);
    }
    
    private void showBookmarkList()
    {
    	AwesomePagerActivity.this.startActivityForResult(new Intent().setClass(AwesomePagerActivity.this, BookmarkList.class),0);
    }
    
    private void showBookmarkAdd()
    {
		if(ev != null)
		{
	        ev.setText(getSurahName(getCurrentPage())+", Page: "+(getCurrentPage()));
	    }
		AwesomePagerActivity.this.showDialog(DIALOG_SET_BOOKMARK);
    }
	
	private void saveSettings(){
		if(waitForLayout)
			return;		

		preferenceEditor.putInt("currentPage", getCurrentPage()); 
		preferenceEditor.putString("bookmarks",new Gson().toJson(bookmarks));
		preferenceEditor.putInt("zoom_option_landscape", zoom_option[ORIENTATION_LANDSCAPE]);
		preferenceEditor.putInt("zoom_option_portrait", zoom_option[ORIENTATION_PORTRAIT]);
		preferenceEditor.putBoolean("zoom_controls_landscape", zoom_controls[ORIENTATION_LANDSCAPE]);
		preferenceEditor.putBoolean("zoom_controls_portrait", zoom_controls[ORIENTATION_PORTRAIT]);
		preferenceEditor.putFloat("appBrightnessValue", appBrightnessValue);
      preferenceEditor.putBoolean("enableFullScreen", enableFullScreen);
		preferenceEditor.commit();
		backupManager.dataChanged();
		
	}
	
	@Override
	protected void onPause() {
		saveSettings();
		super.onPause();
	}
	

  private boolean isType(String key, Class cls)
  {
    if(!preferences.contains(key))
      return false;
    try{

      if(cls == Integer.class)
        preferences.getInt(key, 0);
      else if(cls == Float.class)
        preferences.getFloat(key, 0);
      else if(cls == Long.class)
        preferences.getLong(key, 0);

      return true;
    }catch (ClassCastException ex){
      return false;
    }
  }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	//    	    	
    	if (saveSettings)
    	{
    		saveSettings();
    		saveSettings = false;
    	}

          if(requestCode == 0x123F)
    {
      if(data == null || data.getData() == null) {
        Toast.makeText(this, "Backup canceled ",Toast.LENGTH_LONG).show();
        return;
      }
      saveHighlights();
      String json = new Gson().toJson(preferences.getAll(), new TypeToken<Map<String, ?>>() {}.getType());
      try {
        ParcelFileDescriptor pfd =
            this.getContentResolver().
                openFileDescriptor(data.getData(), "w");

        FileOutputStream fileOutputStream =
            new FileOutputStream(
                pfd.getFileDescriptor());
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.getFD().sync();
        fileOutputStream.close();
        pfd.close();
        Toast.makeText(this, "Backup was successful",Toast.LENGTH_LONG).show();
      } catch (Exception e) {
        e.printStackTrace();
        Toast.makeText(this, "Backup Error: "+e.getMessage(),Toast.LENGTH_LONG).show();
      }
      return;
    }
    else if(requestCode == 0x123E)
    {
      if(data == null || data.getData() == null)
        return;
      try {
        InputStream inputStream =
            getContentResolver().openInputStream(data.getData());
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        Map<String, ?> pMap = new Gson().fromJson(br, new TypeToken<Map<String, ?>>() {}.getType());

        for(String k: pMap.keySet())
        {
          Class cls = pMap.get(k).getClass();
          if( cls == Boolean.class)
            preferenceEditor.putBoolean(k, Boolean.valueOf(pMap.get(k).toString()));
          else if(cls == Float.class)
            preferenceEditor.putFloat(k, Float.valueOf(pMap.get(k).toString()));
          else if(cls == Integer.class)
            preferenceEditor.putInt(k, Integer.valueOf(pMap.get(k).toString()));
          else if(cls == String.class)
            preferenceEditor.putString(k, pMap.get(k).toString());
          else if(cls == Long.class)
            preferenceEditor.putLong(k, Long.valueOf(pMap.get(k).toString()));
          else if(cls == Double.class)
          {
            if(isType(k, Integer.class))
              preferenceEditor.putInt(k, (int)Double.parseDouble(pMap.get(k).toString()));
            else if(isType(k, Float.class))
              preferenceEditor.putFloat(k, Float.valueOf(pMap.get(k).toString()));
            else if(isType(k, Long.class))
              preferenceEditor.putLong(k, (long)Double.parseDouble(pMap.get(k).toString()));
          }
        }
        preferenceEditor.commit();
        Toast.makeText(this, "Restore was successful",Toast.LENGTH_LONG).show();
        doSelfRestart();
      } catch (IOException e) {
        Toast.makeText(this, "Restore Error: "+e.getMessage(),Toast.LENGTH_LONG).show();
      }
      return;
    }

    	//Toast.makeText(this, "onActivityResult ",Toast.LENGTH_LONG).show();
        if (index_activity_response >= 0xABC00 && index_activity_response <= 0xABC03)
        {
            processSettings(index_activity_response);
        }    	
        else if( index_activity_response > 0)
    	{
			String ApkFileName = Helpers.getExpansionAPKFileName(this, APKDownloaderActivity.xAPKS[0].mIsMain, APKDownloaderActivity.xAPKS[0].mFileVersion);
			try{
			zrf = new ZipResourceFile(Helpers.generateSaveFileName(this, ApkFileName));
 		    
			ApkFileName = Helpers.getExpansionAPKFileName(this, APKDownloaderActivity.xAPKS[1].mIsMain, APKDownloaderActivity.xAPKS[1].mFileVersion);
			zrf_ext = new ZipResourceFile(Helpers.generateSaveFileName(this, ApkFileName));
			
			}catch(Exception e){e.printStackTrace();}

			setCurrentPage(index_activity_response);
    		if(databaseExist == false)
    		{
                  if(preferences.getBoolean("ShowTutorial", true) ) {
          startActivity(new Intent().setClass(AwesomePagerActivity.this, WalkthroughActivity.class));
          preferenceEditor.putBoolean("ShowTutorial", false);
        }

    			databaseExist = true;
    			preferenceEditor.putBoolean("APKFileExist", true);
    			preferenceEditor.commit();
                  setCurrentPage(requestCode); //request contains page to show
    		    
    		}
    	}
    	else if( index_activity_response == -1111) //if download abort occurs
    	{
    		finish();
    		
    	}
    	
    	
        }
    
	@Override
 	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(volKeysEnabled)
		{
	       	if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
	       	{
	           	doNextPage();
	           	return true;        		
	       	}
	       	else if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
	       	{
	       		doPreviousPage();        	
	       		return true;
	       	}
		}
        
        return super.onKeyDown(keyCode, event);
    }
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (volKeysEnabled && (keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN))
		       return true;
		else
			return super.onKeyUp(keyCode, event);
	}
    
    Toast toastNext =null, toastPrevious=null, cornerLeft=null, cornerRight=null;
    
    public void doNextPage()
    {
        //Toast.makeText(getApplicationContext(), this.displayWidth+" x "+this.displayHeight, Toast.LENGTH_LONG).show();
        int p = getCurrentPage();
        setCurrentPage(p+1);
        if(toastNext == null)
        {
        	LayoutInflater inflater = LayoutInflater.from(this);
        	View layout = inflater.inflate(R.layout.toast_left, null);
        	toastNext = new Toast(getApplicationContext());        	
        	toastNext.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        	toastNext.setDuration(Toast.LENGTH_SHORT);
        	toastNext.setView(layout);
        }

        toastNext.show();
        //show toast for 1 sec instead of 2 sec (LENGTH_SHORT)
        new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {toastNext.cancel();}
        }, 600);
 
    }
    
    public void doPreviousPage()
    {
        int p = getCurrentPage();
        setCurrentPage(p-1);
        if(toastPrevious == null)
        {
        	LayoutInflater inflater = LayoutInflater.from(this);
        	View layout = inflater.inflate(R.layout.toast_right, null);
        	toastPrevious = new Toast(getApplicationContext());        	
        	toastPrevious.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        	toastPrevious.setDuration(Toast.LENGTH_SHORT);
        	toastPrevious.setView(layout);
        }
        toastPrevious.show();
        //show toast for 1 sec instead of 2 sec (LENGTH_SHORT)
        new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {toastPrevious.cancel();}
        }, 600);

   }   
    
    public void setCurrentPage(int i)
    {
    	awesomePager.setCurrentItem(NUM_AWESOME_VIEWS-i);
    }

    public int getCurrentPage()
    {
    	if(awesomePager != null)
    		return NUM_AWESOME_VIEWS - awesomePager.getCurrentItem();
    	else
    		return 1;
    }

    
    private String readAll2(BufferedReader rd, int displayWidth, int displayHeight, int page) throws IOException {
        StringBuilder sb = new StringBuilder();
        String inputLine, header="";
 
        String  html = "<html><head> <meta name=\"viewport\" content=\"target-densitydpi=device-dpi, user-scalable=yes\" /></head>";
                //html += "<style>svg, #container{width: 100%; height: auto}</style><div id=\"container\"><body>\n";
        		html += "<body>";
 
         rd.readLine();rd.readLine(); //ignore first 2 lines
         
         while ((inputLine = rd.readLine()) != null){
             if(inputLine.charAt(1) == 's' && inputLine.contains("<svg")) //Locate <svg
             {
                 int ptr = inputLine.indexOf("width=");
                 int pageWidth = (int)(Integer.valueOf(inputLine.substring(ptr+7, inputLine.indexOf("\"", ptr+8)-2))*1.3333);
                 ptr = inputLine.indexOf("height=");               
                 int pageHeight = (int)(Integer.valueOf(inputLine.substring(ptr+8, inputLine.indexOf("\"", ptr+8)-2))*1.3333);

                 
                 SVGStrCache_header[page%3] =  inputLine;
                 
                 if(displayHeight>0)
                 {
                	 
                	 svgScale[page%3][0] = (float)displayWidth/pageWidth;
                	 svgScale[page%3][1] =  (float)displayHeight/pageHeight;
                     html += "<img height=\""+(displayHeight)+"\" width=\""+displayWidth+"\" ";
                     
                 }
                 else
                {
                     float scale = (float)displayWidth/pageWidth; 
                     svgScale[page%3][0] = scale;
                     svgScale[page%3][1] = scale;
                     html += "<img width=\""+displayWidth+"\" ";
                 }
                  
                 if(highligts.get(page) != null && highligts.get(page).size() >0)
                 {
                	 for(int i=0; i<highligts.get(page).size(); i++)
                		 inputLine += ((HighlightRect)highligts.get(page).get(i)).getRect();
                 }
                 
                 header = inputLine;
             }
             else if(inputLine.charAt(1) == 'g' && inputLine.contains("id="))  //Locate <g
             {
                     inputLine = "<g id=\"#060606ff\" fill=\""+colors[colorScheme][1]+"\">\n";
                     sb.append(inputLine);
             }
             else
                 	sb.append(inputLine);
         }
 
       	 SVGStrCache_body[page%3] =  sb.toString();

         sb.insert(0, header);
		//html += " src=\"data:image/svg+xml;base64,"+Base64.encodeToString(sb.toString().getBytes(), Base64.DEFAULT)+"\"></img></body></html>";
		html += " src=\'data:image/svg+xml;utf8,"+sb.toString()+"\'></img></body></html>";
         //html += sb.toString() + "</div></body></html>";
       // Log.d("",html);
         return html;
    }
    
    
/*    
    private String readAll(BufferedReader rd, int displayWidth, int displayHeight, int page) throws IOException {
        StringBuilder sb = new StringBuilder();
        String inputLine;
 
        String  html = "<html><head> <meta name=\"viewport\" content=\"target-densitydpi=device-dpi,minimum-scale=1, user-scalable=yes\" />";
                html += "<style TYPE=\"text/css\">\n"; 
                html += "#oo{";
                
        //html = "<!DOCTYPE html><head><meta charset=\"UTF-8\" /></head><body><h1>Embedded SVG</h1>";//<svg width=\"300px\" height=\"300px\" xmlns=\"http://www.w3.org/2000/svg\"><circle cx=\"50\" cy=\"50\" r=\"40\" stroke-width=\"4\" fill=\"yellow\" /><text x=\"10\" y=\"50\" font-size=\"30\">My SVG</text></svg></body></html>";        
        //html += "<svg width=\"1284pt\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1284 2001\" version=\"1.1\" height=\"2001pt\"><g id=\"#000ff\"><circle cx=\"50\" cy=\"50\" r=\"40\" stroke-width=\"4\" fill=\"yellow\" />";
        //html += "<path d=\"m1146.4 26.08c0.9-1.32 1.8-2.63 2.7-3.93 2.9 5.7 4.8 12.79 0.9 18.51-0.8-4.96-1.9-9.87-3.6-14.58z\"/><path d=\"m1120.2 25.27c0.6-0.05 1.7-0.14 2.2-0.19 0.4 5.34 1.9 10.54 2 15.9 0.5 8.01 4.5 15.8 3 23.92-3 4.49-9 3.8-13.6 4.6-4.9 0.27-9.8 3.42-14.6 1.03 0.5-1.58 1.1-3.16 1.6-4.74 7.9-0.82 15.9-1.8 23.7-3.64-2.4-12.13-6.3-24.38-4.3-36.88z\"/><path d=\"m1076 29.62c2.9 2.51 2.8 6.67 3.1 10.15-0.9 1.79-3 2.38-4.6 3.38 1.8 6.73 4.3 13.51 4 20.59-0.3 3.96-3.2 8.09-7.5 7.96-5 0.07-8.6-3.87-11.2-7.75-2.4 3.18-4.5 7.64-8.9 8.12-4.1-0.14-8.5-2.24-10.1-6.2-0.9-4.11-1-8.73 1.3-12.42 0.5 3.98-0.1 9.16 4 11.44 2.9 2.67 7.3 1.81 9.6-1.22 4.5-4.4-0.2-10.65 1-15.91 1.6 0.64 2.6 2 3 3.68 1.5 4.61 2.7 9.91 6.9 12.91 2.5 1.97 5.9 1.22 8.8 1.08 0.7-7.92-3-15.17-4.5-22.77-2.3 0.33-5.4 1.64-6.8-1.03-1.7-3.61 0.3-7.88 2.7-10.72 0 3.07-0.3 6.14-0.2 9.21 3.6-1.36 4.3-5.32 6.1-8.21 0.8 2.02 1.6 4.07 2.3 6.13 0.1-2.83 0.4-5.65 1-8.42z\"/></g></svg></body></html>";
        //html += "<path d=\" M 10 25 L 10 75 L 60 75 L 10 25\"/></g></svg></body></html>";
        //html = "<html><head> <meta name=\"viewport\" content=\"target-densitydpi=device-dpi,minimum-scale=1, user-scalable=yes\" >";
        //html += "<style>body  { background-image:url(\"data:image/svg+xml;base64,";//PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+PGxpbmVhckdyYWRpZW50IGlkPSdncmFkaWVudCc+PHN0b3Agb2Zmc2V0PScxMCUnIHN0b3AtY29sb3I9JyNGMDAnLz48c3RvcCBvZmZzZXQ9JzkwJScgc3RvcC1jb2xvcj0nI2ZjYycvPiA8L2xpbmVhckdyYWRpZW50PjxyZWN0IGZpbGw9J3VybCgjZ3JhZGllbnQpJyB4PScwJyB5PScwJyB3aWR0aD0nMTAwJScgaGVpZ2h0PScxMDAlJy8+PC9zdmc+\");}</style></head></html>";
        //html = "<HTML>    <HEAD>        <TITLE>           A Small Hello        </TITLE>     </HEAD>  <BODY>     <H1>Hi</H1>     <P>This is very minimal \"hello world\" HTML document.</P>  </BODY>  </HTML>";
        //html = "<html><head> <meta name=\"viewport\" content=\"target-densitydpi=device-dpi,minimum-scale=1, user-scalable=yes\" /><body>";
         
         rd.readLine();rd.readLine(); //ignore first 2 lines
         
         while ((inputLine = rd.readLine()) != null){
             if(inputLine.charAt(1) == 's') //Locate <svg
             {
                 int ptr = inputLine.indexOf("width=");
                 float pageWidth = (Integer.valueOf(inputLine.substring(ptr+7, inputLine.indexOf("\"", ptr+8)-2))*1.3333f)+50;
                 ptr = inputLine.indexOf("height=");               
                 float pageHeight = (Integer.valueOf(inputLine.substring(ptr+8, inputLine.indexOf("\"", ptr+8)-2))*1.3333f)+50;
                 
                 
                 if(displayHeight>0)
                 {
                	 svgScale[page%3][0] = (float)displayWidth/pageWidth;
                	 svgScale[page%3][1] =  (float)displayHeight/pageHeight;
                     //html += "height:"+displayHeight+"px; width:"+displayWidth+"px;";
                     //inputLine += "<g transform=\"scale("+((float)displayWidth/pageWidth)+" "+((float)displayHeight/pageHeight)+")\">\n";                  
                     
                 }
                 else
                 {
                     float scale = (float)displayWidth/pageWidth; 
                     svgScale[page%3][0] = scale;
                     svgScale[page%3][1] = scale;
                     //html += "height:"+((int)(pageHeight*scale))+"px; width:"+displayWidth+"px;";
                     //html += "width:"+displayWidth+"px;";
                     //inputLine += "<g transform=\"scale("+scale+")\">\n";                    
                 }
                 
                 if(highligts.get(page) != null && highligts.get(page).size() >0)
                 {
                	 for(int i=0; i<highligts.get(page).size(); i++)
                		 inputLine += ((HighlightRect)highligts.get(page).get(i)).getRect();
                 }
                 
             }
             else if(inputLine.charAt(1) == 'g' || (inputLine.charAt(2) == 'g'))  //Locate <g
             {
                     inputLine = "<g id=\"#060606ff\" fill=\""+colors[colorScheme][1]+"\">\n";//"<g id=\"#ff0000ff\"  fill=\"#33B5E5\">";
             }
             //else if(inputLine.charAt(1) == '/' && inputLine.charAt(2) == 's')  //Locate </svg
             //    inputLine = "</g></svg>";
 
                 sb.append(inputLine);
         }
 
         html += "overflow:hidden; }\n";
         html += "</style></head><body>\n";         
         html += "<div id=\"oo\" >\n";       
 
         html += sb.toString()+"</div></body></html>\n";
         
         //html += sb.toString()+"</body></html>\n";
         //Log.d("a", sb.toString());
         //html += android.util.Base64.encodeToString(sb.toString().getBytes(), android.util.Base64.DEFAULT)+ "\");}</style></head></html>";
         //html += sb.toString()+"</body></html>";
         return html;
    }
	*/

	private String readAll(BufferedReader rd, int displayWidth, int displayHeight, int page) throws IOException {
		StringBuilder sb = new StringBuilder();
		String inputLine, header="";

		String  html = "<html><head> <meta name=\"viewport\" content=\"target-densitydpi=device-dpi, user-scalable=yes\" />";
		html += "<style TYPE=\"text/css\">\n";
		html += "#oo{";

		String textColor = colors[colorScheme][1];

		rd.readLine();rd.readLine(); //ignore first 2 lines

		while ((inputLine = rd.readLine()) != null){

			if( inputLine.charAt(0) == ' ' )
				inputLine = inputLine.trim();

			if(inputLine.charAt(1) == 's' && inputLine.contains("<svg")) //Locate <svg
			{
				int ptr = inputLine.indexOf("width=");
				String pageWidthStr = inputLine.substring(ptr+7, inputLine.indexOf("\"", ptr+8)-2);
				int pageWidth = (int)(Integer.valueOf(pageWidthStr)*1.3333);
				ptr = inputLine.indexOf("height=");
				String pageHeightStr = inputLine.substring(ptr+8, inputLine.indexOf("\"", ptr+8)-2);
				int pageHeight = (int)(Integer.valueOf(pageHeightStr)*1.3333);

				//SVGStrCache_header[page%3] =  null;

				if(displayHeight>0)
				{
					svgScale[page%3][0] = (float)displayWidth/pageWidth;
					svgScale[page%3][1] =  (float)displayHeight/pageHeight;

					html += "height:"+displayHeight+"px; width:"+displayWidth+"px;";
					inputLine += "<g transform=\"scale("+((float)displayWidth/pageWidth)+" "+((float)displayHeight/pageHeight)+")\">\n";

				}
				else
				{
					float scale = (float)displayWidth/pageWidth;
					svgScale[page%3][0] = scale;
					svgScale[page%3][1] = scale;

					html += "height:"+((int)(pageHeight*scale))+"px; width:"+displayWidth+"px;";
					inputLine = inputLine.replace(pageWidthStr, ""+((int)(scale*pageWidth)));
					inputLine = inputLine.replace(pageWidthStr, ""+((int)(scale*pageWidth)));
					inputLine = inputLine.replace(pageHeightStr, ""+((int)(scale*pageHeight)));
					inputLine = inputLine.replace(pageHeightStr, ""+((int)(scale*pageHeight)));
					inputLine += "<g transform=\"scale("+scale+")\">\n";
				}
				if(highligts.get(page) != null && highligts.get(page).size() >0)
				{
					for(int i=0; i<highligts.get(page).size(); i++)
						inputLine += ((HighlightRect)highligts.get(page).get(i)).getRect();
				}

			}
			else if(inputLine.charAt(1) == 'g' && inputLine.contains("id="))  //Locate <g
			{
				inputLine = "<g id=\"#060606ff\" fill=\""+textColor+"\">\n";//"<g id=\"#ff0000ff\"  fill=\"#33B5E5\">";
			}
			else if(inputLine.charAt(1) == '/' && inputLine.charAt(2) == 's')  //Locate </svg
				inputLine = "</g></g></svg>";

			sb.append(inputLine);
		}

		html += "overflow:hidden; }\n";
		html += "</style></head><body>\n";

		html += "<div id=\"oo\" >\n";

		html += sb.toString()+"</div></body></html>\n";

		return html;
	}


	private String svg2img(WebView view, final String srcFileName, boolean cleanTmpDir, boolean saveFiles)throws Exception
    {
    	
      File srcFile = new File(srcFileName);
      String destFile;
      
  	  File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appPath.substring(1,appPath.indexOf("/",2)));
  	  destFile = dir.getPath()+"/"+srcFile.getName()+".jpg";
  	  if(dir.exists() == false)
  		  dir.mkdirs();    	  

      Picture picture = view.capturePicture();
      int w = picture.getWidth()<picture.getHeight()?picture.getWidth():picture.getHeight();
      double scaling = w>800?800.0/w:1.0;
    	
    	
      Bitmap  b = Bitmap.createBitmap( picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);
                                                
        Canvas c = new Canvas( b );
        picture.draw( c );
        
        if(scaling < 1)
        	b = Bitmap.createScaledBitmap(b, (int)(b.getWidth()*scaling), (int)(b.getHeight()*scaling), true);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream( destFile);
            if ( fos != null )
            {
              	b.compress(Bitmap.CompressFormat.JPEG, 70, fos);
                fos.close();                
             }
         }catch( Exception e )
         {
            e.printStackTrace();
         }
         
        //Log.d("<<<<<<<<<<<<<","<<<<<<<<<<<");
              //}    });
        
        return destFile;
    }
    int scrollX, scrollY;
    public void reloadPage(int page)
    {
    	float scale = liveWebView[page%3].getScale();
    	scrollX = liveWebView[page%3].getScrollX();
    	scrollY = liveWebView[page%3].getScrollY();
    	/*
        String  html = "<html><head> <meta name=\"viewport\" content=\"target-densitydpi=device-dpi, user-scalable=yes\" /></head>";
		html += "<body>";
		html += "<img width=\""+displayWidth+"\" ";
		//html += " src=\"data:image/svg+xml;base64,";
		html += " src=\'data:image/svg+xml;utf8,";
		
		String hStr="";
		
        if(highligts.get(page) != null && highligts.get(page).size() >0)
        {
       	 for(int i=0; i<highligts.get(page).size(); i++)
       		hStr += highligts.get(page).get(i).getRect();
        }
		//html += Base64.encodeToString((SVGStrCache_header[page%3]+hStr+SVGStrCache_body[page%3]).getBytes(), Base64.DEFAULT)+"\"></img></body></html>";
		html += SVGStrCache_header[page%3]+hStr+SVGStrCache_body[page%3]+"\'></img></body></html>";
		*/
    	String html = creatHtmlPage(page, displayWidth, -1);
		liveWebView[page%3].setInitialScale((int)(scale*100));
		this.liveWebView[page%3].loadDataWithBaseURL(null, html, "text/html", "utf-8", "");

		liveWebView[page%3].setWebViewClient(new WebViewClient(){

	        @Override
	        public void onPageFinished(WebView view, String url) {
			        // do your stuff here
				   scrollTo(scrollX, scrollY, view);
			    }
			});		
		

    }
    
    private void scrollTo ( int x, int y, WebView  webView) {
    	final int sx =x, sy=y;
    	final WebView v = webView;
        webView.postDelayed( new Runnable () {
            @Override
            public void run() {
                v.scrollTo(sx, sy);
            }
        }, 250);  
    }
    
    public String creatHtmlPage(int page, int displayWidth, int displayHeight){
 
	    String htmlText="";
	    InputStream is;
	    
	    String prefix = page<10?"000":(page<100?"00":page<1000?"0":"");
	    String file = (prefix+page)+".svgz";
	    
	     try {
	    	 if(skipDatabaseCheck == false)
	    	 {
		    	 is = zrf_ext.getInputStream(file);
		    	 if(is == null )
		    		 is = zrf.getInputStream(file);
		    	 
		    	 BufferedReader rd = new BufferedReader (new InputStreamReader(new GZIPInputStream(is)));
		         htmlText = readAll(rd, displayWidth, displayHeight, page);
		         is.close();
	    	 }else
	    	 {
	    		 //BufferedReader rd = new BufferedReader (new InputStreamReader(new GZIPInputStream(new FileInputStream(SDCardRoot.getPath()+this.appPath+file))));//"/test/0029.svgz"))));
	    		 //BufferedReader rd = new BufferedReader (new InputStreamReader(new GZIPInputStream(new FileInputStream(SDCardRoot.getPath()+"/test/0611.svgz"))));
	    		 htmlText = "<html><body><p>Hello world</p></html>"; //readAll(rd, displayWidth, displayHeight, page);
	    		 
	    	 }
	         
	     }catch(Exception ex)
	        {
	    	 StringWriter sw = new StringWriter();
	    	 PrintWriter pw = new PrintWriter(sw);
	    	 ex.printStackTrace(pw);
	    	 	    	 
	         //Log.d("IOException:",file+"\n"+sw.toString());
	         new AlertDialog.Builder(this)
	         .setTitle("Err 404 "+file)
	         .setMessage(sw.toString())
	         .setIcon(android.R.drawable.ic_dialog_alert)
	          .show();
	        }
	     
	     return htmlText;
 }
    
    public static String getSurahName(int page)
    {
    	int i=0;
    	if(page <= 1)
    		return SurahList.surahName_array[0];
    	
    	for(i=0; i<SurahList.surahIndex.length;i++)
    		if( SurahList.surahIndex[i] > page )
    			break;
    	return SurahList.surahName_array[i-1];		
    }
    
    private void addBookmark(final String desc, final int page)
    {
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		
        for(int i=0; i<bookmarks.size(); i++)
		{
			BookmarkObject o = bookmarks.get(i);
			if(o.isFolder)
				arrayAdapter.add(o.desc);
		}
    	
        if(arrayAdapter.isEmpty())
        {
        	int j=0;
        	while(j<bookmarks.size() && bookmarks.get(j).isFolder) j++;
        	bookmarks.add(j, new BookmarkObject(desc, page, false));
        }
        else
        {
    		AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
            builderSingle.setTitle("Choose bookmark folder");
            builderSingle.setIcon(R.drawable.ic_menu_archive);
            builderSingle.setNegativeButton("Cancel", null);
            arrayAdapter.insert("<root>", 0);
            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {
		                @Override
		                public void onClick(DialogInterface dialog, int which) {
		                	String destFolder = arrayAdapter.getItem(which);
		                	if(destFolder.equals("<root>"))
		                	{
		                    	int j=0;
		                    	while(j<bookmarks.size() && bookmarks.get(j).isFolder) j++;		                		
		                		bookmarks.add(j, new BookmarkObject(desc, page, false));
		                	}
		                	else
	                    		for(int i=0; i<bookmarks.size(); i++)
	                    		{
	                    			BookmarkObject o = bookmarks.get(i);
	                    			if(o.isFolder && o.desc.equals(destFolder))
	                    			{
	                    	        	int j=0;
	                    	        	while(j<o.list.size() && o.list.get(j).isFolder) j++;
	                    				o.list.add(j, new BookmarkObject(desc, page, false));;
	                    				break;
	                    			}
	                    		}
		                }
            	
            });        
            builderSingle.show();
        }

    }
    
    @Override
    protected Dialog onCreateDialog(int id)    
    {
    	
        if(id==DIALOG_SET_BOOKMARK)
        {
        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.alert_dialog_text_entry, null);
        ev = (EditText) textEntryView.findViewById(R.id.bookmark_name_edit);
        ev.setText(getSurahName(getCurrentPage())+", Page: "+(getCurrentPage()));
        
        
        return new AlertDialog.Builder(AwesomePagerActivity.this).setIcon(R.drawable.ic_menu_star)
            .setTitle("Name your bookmark")
            .setView(textEntryView)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                	addBookmark(ev.getText().toString().replaceAll("\n", " "), getCurrentPage());
                }
            })
            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    /* User clicked cancel so do some stuff */
                }
            })
            .create();    	

        }else if(id == DIALOG_SAVE)
        {
            AlertDialog.Builder customDialog = new AlertDialog.Builder(AwesomePagerActivity.this);
            
            customDialog.setTitle("Save");
            customDialog.setIcon(R.drawable.ic_menu_save);
            customDialog.setMessage("The page has been saved in Pictures folder.");
            
            customDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
            	 @Override
            	 public void onClick(DialogInterface arg0, int arg1) {
             }});
          
             customDialog.setNegativeButton("Open", new DialogInterface.OnClickListener(){
            	 @Override
            	 public void onClick(DialogInterface arg0, int arg1) {
            		 // TODO Auto-generated method stub
            		 Intent intent = new Intent();
            		 intent.setAction(Intent.ACTION_VIEW);
            		 intent.setDataAndType(Uri.fromFile( new File(savedPageFilePath)), "image/jpeg");
            		 startActivity(intent);            		
            	 }});

             return customDialog.create();
                    
        }else if(id== DIALOG_SHARE)
        {
        	
            AlertDialog.Builder customDialog = new AlertDialog.Builder(AwesomePagerActivity.this);
            
            customDialog.setTitle("Share");
            customDialog.setIcon(R.drawable.ic_menu_share);
                 
              final TextView share_label = new TextView(this); 
              share_label.setText("Select pages to share:");
              share_label.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);

              final CheckBox thisPage_chkBox = new CheckBox(AwesomePagerActivity.this);
              thisPage_chkBox.setText("This Page");
              thisPage_chkBox.setChecked(true);
                
              final CheckBox nextPage_chkBox = new CheckBox(AwesomePagerActivity.this);
              nextPage_chkBox.setText("Next Page");
                
              final CheckBox prevPage_chkBox = new CheckBox(AwesomePagerActivity.this);
              prevPage_chkBox.setText("Previous Page");
              
              
              LinearLayout ll = new LinearLayout(this);
              ll.setOrientation(LinearLayout.VERTICAL);
              ll.setGravity(Gravity.CENTER_VERTICAL);
              
              ll.addView(share_label);
              ll.addView(thisPage_chkBox);
              ll.addView(nextPage_chkBox);
              ll.addView(prevPage_chkBox);
              //ll.addView(shareseekbar_text, layoutParams_shareseekbar_text);
              //ll.addView(share_seekbar);
              
              customDialog.setView(ll);
            
              customDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
              	 @Override
              	 public void onClick(DialogInterface arg0, int arg1) {
                  	Intent intent = new Intent();
                 	intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                 	intent.putExtra(Intent.EXTRA_SUBJECT, "Verses from Surah"+getSurahName(getCurrentPage()).split(":")[1]);
                 	intent.setType("image/jpeg"); // This example is sharing jpeg images. 
                 	
                 	sharedPages = (thisPage_chkBox.isChecked()?1:0) + (nextPage_chkBox.isChecked()?2:0) + (prevPage_chkBox.isChecked()?4:0);

                 	intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, prepareSharedFiles());
                 	startActivity(intent);
               }});
            
               customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
              	 @Override
              	 public void onClick(DialogInterface arg0, int arg1) {
              		 // TODO Auto-generated method stub

              	 }});

               return customDialog.create();
			
        }
        else if(id==DIALOG_SET_BRIGHTNESS)
        {
            AlertDialog.Builder customDialog = new AlertDialog.Builder(AwesomePagerActivity.this);
            
            customDialog.setTitle("Brightness");
            customDialog.setIcon(R.drawable.ic_brightness);
            
            //-----------------------------------------------

        	autoBrightness_chkBox = new CheckBox(AwesomePagerActivity.this);
        	autoBrightness_chkBox.setText("Auto brightness");
        	autoBrightness_chkBox.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if(autoBrightness_chkBox.isChecked()){
                   		layout_for_brightness.screenBrightness = sysBrightnessValue;
                   		getWindow().setAttributes(layout_for_brightness);
                    }else{
                   		layout_for_brightness.screenBrightness = (seekbar_brightness.getProgress()+1)/10.0f;
                   		getWindow().setAttributes(layout_for_brightness);
                    }
                }
            });
            
            
            seekbar_brightness = new SeekBar(this);
            seekbar_brightness.setMax(9);
            seekbar_brightness.setOnSeekBarChangeListener( new OnSeekBarChangeListener(){
           	 public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
           	 {
           		layout_for_brightness.screenBrightness = (0.01f+progress)/10.0f;
           		getWindow().setAttributes(layout_for_brightness);           		
           	 }
           	 public void onStartTrackingTouch(SeekBar seekBar){
            		if( autoBrightness_chkBox.isChecked() == true)
              			autoBrightness_chkBox.setChecked(false);           		 
           	 }
           	 public void onStopTrackingTouch(SeekBar seekBar){}
            });

            seekbar_brightness.setProgress((int)((appBrightnessValue>0?appBrightnessValue:sysBrightnessValue)*10)-1);
        	autoBrightness_chkBox.setChecked(appBrightnessValue <= 0); //Don't move this line of code

            Button b1 = new Button(this);
            b1.setText("-");
            b1.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View arg0) {
            		if( autoBrightness_chkBox.isChecked() == true)
              			autoBrightness_chkBox.setChecked(false);           		 
  					seekbar_brightness.setProgress(seekbar_brightness.getProgress()-1);
    			}
    		});
            
            Button b2 = new Button(this);
            b2.setText("+");
            b2.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View arg0) {
            		if( autoBrightness_chkBox.isChecked() == true)
              			autoBrightness_chkBox.setChecked(false);           		 
   					seekbar_brightness.setProgress(seekbar_brightness.getProgress()+1);
    			}
    		});

            
            LinearLayout.LayoutParams layoutParams_seekbar = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams_seekbar.weight = 1;
            layoutParams_seekbar.gravity = Gravity.CENTER_VERTICAL;

            LinearLayout.LayoutParams layoutParams_slider = new LinearLayout.LayoutParams(
            		LinearLayout.LayoutParams.FILL_PARENT,
            		LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams_slider.setMargins(0, 0, 0, 30);
            
            LinearLayout ll = new LinearLayout(this);
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.setGravity(Gravity.CENTER_VERTICAL);


            customDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
              	 @Override
              	 public void onClick(DialogInterface arg0, int arg1) {
              		if( autoBrightness_chkBox.isChecked() == true )
              			appBrightnessValue = -1;
              		else
                   		appBrightnessValue = (seekbar_brightness.getProgress()+0.01f)/10.0f;
              		//Log.d("OK",""+appBrightnessValue );
               }});
            
               customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
              	 @Override
              	 public void onClick(DialogInterface arg0, int arg1) {
              		 // TODO Auto-generated method stub
                		layout_for_brightness.screenBrightness = appBrightnessValue>=0?appBrightnessValue:sysBrightnessValue;
                		getWindow().setAttributes(layout_for_brightness);

              	 }});
                  
            
            LinearLayout ll2 = new LinearLayout(this);
            
            ll2.addView(b1);
            ll2.addView(seekbar_brightness,layoutParams_seekbar);
            ll2.addView(b2);
            
            ll.addView(autoBrightness_chkBox);
            ll.addView(ll2);
            customDialog.setView(ll);
            
            return customDialog.create();
            
        	
        }else if(id== DIALOG_ZOOM_OPTION_LANDSCAPE)
        {
            AlertDialog.Builder customDialog = new AlertDialog.Builder(AwesomePagerActivity.this);
            
            customDialog.setTitle("Page layout - LANDSCAPE");// Surah "+SurahList.surahName_array[getSurahIndexFromPage(getCurrentPage())].split(":")[1]);
            customDialog.setIcon(R.drawable.ic_menu_zoom);

            pZoomButtonGroup = new RadioGroup(AwesomePagerActivity.this);
            pZoomButtonGroup.setOrientation(RadioGroup.VERTICAL);
            
            RadioButton fitWidthRadioButton = new RadioButton(AwesomePagerActivity.this);
            fitWidthRadioButton.setText("Fit width");
            fitWidthRadioButton.setId(R.id.id_fit_width);
            fitWidthRadioButton.setChecked(zoom_option[ORIENTATION_LANDSCAPE]==fitWidthRadioButton.getId());

            RadioButton fillScreenRadioButton = new RadioButton(AwesomePagerActivity.this);
            fillScreenRadioButton.setText("Fill screen");
            fillScreenRadioButton.setId(R.id.id_fill_screen);
            fillScreenRadioButton.setChecked(zoom_option[ORIENTATION_LANDSCAPE]==1);
            
            RadioButton orignalRadioButton = new RadioButton(AwesomePagerActivity.this);
            orignalRadioButton.setText("Orignal size");
            orignalRadioButton.setId(R.id.id_original_size);
            orignalRadioButton.setChecked(zoom_option[ORIENTATION_LANDSCAPE]==2);
        	zoom_option_req  = zoom_option[ORIENTATION_LANDSCAPE]; //default value

            LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                    RadioGroup.LayoutParams.WRAP_CONTENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(30, 0, 0, 0);
            
            pZoomButtonGroup.addView(fitWidthRadioButton, layoutParams); 
            pZoomButtonGroup.addView(fillScreenRadioButton, layoutParams); 
        	if( displayWidth >= 700 && displayHeight >= 990)
        		pZoomButtonGroup.addView(orignalRadioButton, layoutParams); 
       
        	pZoomButtonGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
           	 public void onCheckedChanged(RadioGroup group, int checkedId) {
				 zoom_option_req = checkedId==R.id.id_fit_width?0:checkedId==R.id.id_fill_screen?1:2;
           	  }
            });
        	zoomCtr_chkBox_ls = new CheckBox(AwesomePagerActivity.this);
        	zoomCtr_chkBox_ls.setText("Show zoom controls during scrolling");
        	zoomCtr_chkBox_ls.setChecked(zoom_controls[ORIENTATION_LANDSCAPE]);
        	    	
            customDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
           	 @Override
           	 public void onClick(DialogInterface arg0, int arg1) {
            	 if( zoom_option[ORIENTATION_LANDSCAPE] != zoom_option_req)
            	 {
            		 zoom_option[ORIENTATION_LANDSCAPE]   = zoom_option_req;
            		 //Log.d("Zoom Option",""+zoom_option);
            		 awesomePager.getAdapter().notifyDataSetChanged();
            	 }
        		 zoom_controls[ORIENTATION_LANDSCAPE] = zoomCtr_chkBox_ls.isChecked();        		 
            	 webViewReference.setZoomCtrl(zoom_controls[ORIENTATION_LANDSCAPE]);
//            	 Log.d("Zoom Ctrl Option - Landscape "+orientation,""+zoom_controls[orientation]);            	 
            }});
         
            customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
           	 @Override
           	 public void onClick(DialogInterface arg0, int arg1) {
           		 // TODO Auto-generated method stub
            }});
               
            LinearLayout ll = new LinearLayout(AwesomePagerActivity.this);
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.addView(pZoomButtonGroup);
            ll.addView(zoomCtr_chkBox_ls);
            
            customDialog.setView(ll);
            
            return customDialog.create();
        	
    }else if(id== DIALOG_ZOOM_OPTION_PORTRAIT)
    {
        AlertDialog.Builder customDialog = new AlertDialog.Builder(AwesomePagerActivity.this);
        
        customDialog.setTitle("Page Layout - PORTRAIT");
        customDialog.setIcon(R.drawable.ic_menu_zoom);

        pZoomButtonGroup = new RadioGroup(AwesomePagerActivity.this);
        pZoomButtonGroup.setOrientation(RadioGroup.VERTICAL);
        
        RadioButton fitWidthRadioButton = new RadioButton(AwesomePagerActivity.this);
        fitWidthRadioButton.setText("Fit width");
        fitWidthRadioButton.setId(R.id.id_fit_width);
        fitWidthRadioButton.setChecked(zoom_option[ORIENTATION_PORTRAIT]==0);

        RadioButton fillScreenRadioButton = new RadioButton(AwesomePagerActivity.this);
        fillScreenRadioButton.setText("Fill screen");
        fillScreenRadioButton.setId(R.id.id_fill_screen);
        fillScreenRadioButton.setChecked(zoom_option[ORIENTATION_PORTRAIT]==1);
        
        RadioButton orignalRadioButton = new RadioButton(AwesomePagerActivity.this);
        orignalRadioButton.setText("Orignal size");
        orignalRadioButton.setId(R.id.id_original_size);
        orignalRadioButton.setChecked(zoom_option[ORIENTATION_PORTRAIT]==2);
    	zoom_option_req  = zoom_option[ORIENTATION_PORTRAIT]; //default value

        LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.WRAP_CONTENT,
                RadioGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(30, 0, 0, 0);
        
        pZoomButtonGroup.addView(fitWidthRadioButton, layoutParams); 
        pZoomButtonGroup.addView(fillScreenRadioButton, layoutParams); 
    	if( displayWidth >= 700 && displayHeight >= 990)
    		pZoomButtonGroup.addView(orignalRadioButton, layoutParams); 
   
    	pZoomButtonGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
       	 public void onCheckedChanged(RadioGroup group, int checkedId) {
			 zoom_option_req = checkedId==R.id.id_fit_width?0:checkedId==R.id.id_fill_screen?1:2;
       	  }
        });
        
    	zoomCtr_chkBox_pt = new CheckBox(AwesomePagerActivity.this);
    	zoomCtr_chkBox_pt.setText("Show zoom controls during scrolling");
    	zoomCtr_chkBox_pt.setChecked(zoom_controls[ORIENTATION_PORTRAIT]);

    	    	
        customDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
       	 @Override
       	 public void onClick(DialogInterface arg0, int arg1) {
        	 if( zoom_option[ORIENTATION_PORTRAIT] != zoom_option_req)
        	 {
        		 zoom_option[ORIENTATION_PORTRAIT]   = zoom_option_req;
//        		 Log.d("Zoom Option",""+zoom_option);
        		 awesomePager.getAdapter().notifyDataSetChanged();
        	 }
    		 zoom_controls[ORIENTATION_PORTRAIT] = zoomCtr_chkBox_pt.isChecked();        		 
        	 webViewReference.setZoomCtrl(zoom_controls[ORIENTATION_PORTRAIT]);
//        	 Log.d("Zoom Ctrl Option - Portrait",""+zoom_controls[orientation]);
        }});
     
        customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
       	 @Override
       	 public void onClick(DialogInterface arg0, int arg1) {
       		 // TODO Auto-generated method stub
        }});
           
        LinearLayout ll = new LinearLayout(AwesomePagerActivity.this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(pZoomButtonGroup);
        ll.addView(zoomCtr_chkBox_pt);
        
        customDialog.setView(ll);
        
        return customDialog.create();

    }else if(id== DIALOG_GO)
    {
    	return gotoDialog(this);
    }

        return null;        
       }
        
    public AlertDialog gotoDialog(final Context ctx)
    {
    	int surahIndex = getCurrentSurahIndex();
    	
        seekbar = new SeekBar(ctx);
        final AlertDialog.Builder customDialog = new AlertDialog.Builder(ctx);
        
        customDialog.setTitle("Go to");// Surah "+SurahList.surahName_array[getSurahIndexFromPage(getCurrentPage())].split(":")[1]);
        customDialog.setIcon(R.drawable.ic_menu_goto);

        goButtonGroup = new RadioGroup(ctx);
        goButtonGroup.setOrientation(RadioGroup.HORIZONTAL);
        
        RadioButton ayatRadioButton = new RadioButton(ctx);
        ayatRadioButton.setText("Ayat");
        ayatRadioButton.setId(R.id.id_ayat);
        ayatRadioButton.setChecked(true);

        RadioButton RukuRadioButton = new RadioButton(ctx);
        RukuRadioButton.setText("Ruku");
        RukuRadioButton.setId(R.id.id_ruku);

        LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.WRAP_CONTENT,
                RadioGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);
        
        goButtonGroup.addView(ayatRadioButton, layoutParams); 
        goButtonGroup.addView(RukuRadioButton, layoutParams); 
   
        goButtonGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
       	 public void onCheckedChanged(RadioGroup group, int checkedId) {
       		 if(checkedId == R.id.id_ayat) //Ayat radio button
       			 seekbar.setMax(totalAyaat[getCurrentSurahIndex()]-1);
       		 else
       			 seekbar.setMax(totalRuku[getCurrentSurahIndex()]-1);
       		 seekbar.setProgress(0);
       	  }
        });
        
        customDialog.setPositiveButton("Go", new DialogInterface.OnClickListener(){
       	 @Override
       	 public void onClick(DialogInterface arg0, int arg1) {
       		 
       		 if(goButtonGroup.getCheckedRadioButtonId() == R.id.id_ayat) //Ayat radio button
       			 setCurrentPage(getPageNumberForAyat(seekbar.getProgress()+1, getCurrentSurahIndex()));
       		 else
       			 setCurrentPage(getPageNumberForRuku(seekbar.getProgress()+1, getCurrentSurahIndex()));
        }});
     
        customDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
       	 @Override
       	 public void onClick(DialogInterface arg0, int arg1) {
       		 // TODO Auto-generated method stub
        }});
   
        final TextView txtView_go_number = new TextView(ctx);
        txtView_go_number.setText(Html.fromHtml("<b>1</b>") );
        txtView_go_number.setGravity(Gravity.CENTER_HORIZONTAL);
        txtView_go_number.setTextColor(Color.WHITE);
        
        txtView_go_surahName = new Button(ctx);
        //Log.d("go_to",getSurahName(getCurrentPage()));
        txtView_go_surahName.setText(" Surah"+SurahList.surahName_array[surahIndex].split(":")[1]);  
        txtView_go_surahName.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams2.weight = 1;
        layoutParams2.gravity = Gravity.CENTER_VERTICAL;

        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams3.weight = 1;
        layoutParams3.width = 0;

        seekbar.setMax(totalAyaat[surahIndex]-1);
        seekbar.setOnSeekBarChangeListener( new OnSeekBarChangeListener(){
       	 public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
       	 {
       	 	txtView_go_number.setText(Html.fromHtml("<b>" + (progress+1) + "</b>" ));
       	 }
       	 public void onStartTrackingTouch(SeekBar seekBar){}
       	 public void onStopTrackingTouch(SeekBar seekBar){}
        });
        
        LinearLayout ll = new LinearLayout(ctx);
        ll.setOrientation(LinearLayout.VERTICAL);
        LinearLayout ll2 = new LinearLayout(ctx);
        

        Button b1 = new Button(ctx);
        b1.setText("-");
        b1.setOnTouchListener(new LongTouchActionListener (ctx) {
			@Override
			public void onClick(View arg0) {
				seekbar.setProgress(seekbar.getProgress()-1);
			}
			@Override
			public void onLongTouchAction(View v) {
				seekbar.setProgress(seekbar.getProgress()-1); 		    
			} 			
		});
        
        Button b2 = new Button(ctx);
        b2.setText("+");
        b2.setOnTouchListener(new LongTouchActionListener (ctx) {
			@Override
			public void onClick(View arg0) {
				seekbar.setProgress(seekbar.getProgress()+1);
			}
			@Override
			public void onLongTouchAction(View v) {
				seekbar.setProgress(seekbar.getProgress()+1); 		    
			} 			
		});
        
        Button b3 = new Button(ctx);
        b3.setText("Index");
        
        ll2.addView(b1,layoutParams3);
        ll2.addView(b2,layoutParams3);
        ll.addView(txtView_go_surahName);
        ll.addView(goButtonGroup);
        ll.addView(txtView_go_number);
        ll.addView(seekbar,layoutParams2);
        ll.addView(ll2);
        
        customDialog.setView(ll);
        final AlertDialog aDialog = customDialog.create();
        txtView_go_surahName.setOnClickListener(new OnClickListener() {
	    	@Override
			public void onClick(View arg0) {
				aDialog.cancel();
				AwesomePagerActivity.this.startActivityForResult(new Intent().setClass(AwesomePagerActivity.this, TabActivity.class),0);
			}
		});
        return aDialog;
    	
    }
        
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
  }    

    @SuppressLint("NewApi")
	private int getOrientation()
    {
	    DisplayMetrics displayMetrics = new DisplayMetrics();
	    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		hardwareMenuKeyPresent = false;
	    if(displayMetrics.widthPixels > displayMetrics.heightPixels)
	    	orientation = ORIENTATION_LANDSCAPE;
	    else
	    	orientation = ORIENTATION_PORTRAIT;

	    return orientation;
	 }

    public boolean isLargeDisplay(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
      }    
    
    private ArrayList<Uri> prepareSharedFiles()
    {
    	String tmpFileName = "";
    	ArrayList<Uri> files = new ArrayList<Uri>();
    	boolean cleanTmpDir=true;
    	int p = getCurrentPage();
    	
    	//Log.d("prepareSharedFiles", ""+sharedPages);

    	try
    	{
			if((sharedPages & 0x4) >0)
			{
				tmpFileName = svg2img(liveWebView[(p-1)%3], 	 ""+(p-1), cleanTmpDir, false);
					
	    		files.add(Uri.fromFile(new File(tmpFileName)));
	    		cleanTmpDir = false;
			}
			
			if((sharedPages & 0x1) >0)
			{
				tmpFileName = svg2img(liveWebView[p%3], 	 ""+p, cleanTmpDir, false);
				
	    		files.add(Uri.fromFile(new File(tmpFileName)));
	    		cleanTmpDir = false;
			}
				
			if((sharedPages & 0x2) >0)
			{
				tmpFileName = svg2img(liveWebView[(p+1)%3], 	 ""+(p+1), cleanTmpDir, false);
	    		files.add(Uri.fromFile(new File(tmpFileName)));
	    		cleanTmpDir = false;
			}
    	}catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
			    	
    	return files;

    }

    private void prepareSavedFile()
    {
    	String tmpFileName = "";
    	int p = getCurrentPage();
    	
    	//Log.d("prepareSharedFiles", ""+sharedPages);
    	try{
			tmpFileName = svg2img(liveWebView[p%3], ""+p, false, true);

    		savedPageFilePath = tmpFileName;
    	}catch(Exception ex)
    	{
    		ex.printStackTrace();
    		savedPageFilePath = null;
    	}
			    	
    	return;

    }
    
  /*  
    private String copyFile(int p, boolean cleanTmpDir, boolean saveFile)
    {
    	  String prefix = p<10?"000":(p<100?"00":(p<1000?"0":""));
    	  String sourceFile = SDCardRoot.getPath()+appPath+prefix+p;

          FileInputStream fis = null;
          FileOutputStream fos = null; //
          File srcFile = new File(sourceFile);
          String destFile;
          File noMediaFile = null;

          if(saveFile == false)
          {

	    	  File dir = new File(srcFile.getParentFile(),"tmp");
	    	  destFile = dir.getPath()+"/"+srcFile.getName()+".jpg";
	    	  noMediaFile = new File(dir, ".nomedia");
	    			  
	    	  if(dir.exists())
	    	  {
	      		  if(cleanTmpDir)
	      		  {
	      			  File[] tmpFiles = dir.listFiles();
	      			  for(int x=0; x<tmpFiles.length;x++)
	      				  tmpFiles[x].delete();
	      		  }
	    	  }else
	    		  dir.mkdir();
          }else
          {
    	  	  File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appPath.substring(1,appPath.indexOf("/",2)));
    	  	  destFile = dir.getPath()+"/"+srcFile.getName()+".jpg";
    	  	  //Log.d("svg2img", destFile);
    	  	  if(dir.exists() == false)
    	  		  dir.mkdirs();    	  
          }
    	  
          try {
              fis = new FileInputStream(sourceFile);
              fos = new FileOutputStream(destFile);
               
              byte[] buffer = new byte[102400];
              int noOfBytes = 0;

              // read bytes from source file and write to destination file
              while ((noOfBytes = fis.read(buffer)) != -1) {
                  fos.write(buffer, 0, noOfBytes);
              }
              
              if(noMediaFile != null && !noMediaFile.exists())
            	  noMediaFile.createNewFile();
          }
          catch (Exception e) {
              e.printStackTrace();
          }
          finally {
              // close the streams using close method
              try {
                  if (fis != null) {
                      fis.close();
                  }
                  if (fos != null) {
                      fos.close();
                  }
              }
              catch (IOException ioe) {
                  System.out.println("Error while closing stream: " + ioe);
              }
          }
          
          return destFile;
      }
*/
    private int getPageNumberForAyat(int ayat, int surahIndex)
    {
    	int front, rear;
    	int targetPage;
    	boolean surahStartsFromOwnPage=true, surahEndsAtOwnPage=true;
    	
    	//Define the search area
    	front = SurahList.surahIndex[surahIndex];
    	if(surahIndex+1 >= SurahList.surahIndex.length)
    		rear = NUM_AWESOME_VIEWS; 
    	else
    		rear  = SurahList.surahIndex[surahIndex+1];
   		
   		for(int x=0; x<surahEndAndStartPage.length;x++)
   			if( front == surahEndAndStartPage[x])
   				surahStartsFromOwnPage = false;
   			else if( rear == surahEndAndStartPage[x])
   			{
   				surahEndsAtOwnPage = false;
   				break;
   			}
    	
   		if(surahEndsAtOwnPage == true)
   			rear--;
   		
   		if(surahStartsFromOwnPage == false)
   			front++;
   		
    	targetPage = rear;
    	for(int i=front; i <=rear; i++)
    	{
			//Log.d("i:"+i,"ayatIndices[i]: "+ayatIndices[i]);
    		if(ayatIndices[i] > ayat )
    		{
    			targetPage = i-1;
    			//Log.d("TargetFound","targetPage:"+targetPage);
    			break;
    		}
    	}
    	
    	if(surahStartsFromOwnPage == false)  //Restore front value
    		front--;
    	
    	if(targetPage < front)
    		targetPage = front;    	
    	else if(targetPage > rear)
    		targetPage = rear;
    	
    	while(ayatIndices[targetPage] == 0)
    		targetPage--;
    	
    	//Log.d("getPageNumberForAyat("+ayat+")","front: "+front+" rear: "+rear+" target:"+targetPage);
    	
    	return targetPage;
    }
    
    private int getPageNumberForRuku(int ruku, int surahIndex)
    {
    	return getPageNumberForAyat(rukuIndex[surahIndex][ruku-1], surahIndex);
    }
    
    private int getCurrentSurahIndex()
    {
    	int surahIndex=113;
    	int currentPage = getCurrentPage();

    	//check which surah is under consideration
    	for( int i=0;i<SurahList.surahIndex.length;i++)
    		if(SurahList.surahIndex[i] > currentPage)
    		{
    			surahIndex = i-1;
    			break;
    		}    	
    	return surahIndex<0?0:surahIndex;
    }

    private int currentProgressbarPage=-1;
    private void setProgressParah(){
    	int parahIndex=29;
    	int currentPage = getCurrentPage();
    	if(currentProgressbarPage == currentPage)
    		return;

    	currentProgressbarPage = currentPage;
    	
    	for( int i=0;i<ParahList.parahIndex.length;i++)
    		if(ParahList.parahIndex[i] > currentPage)
    		{
    			parahIndex = i-1;
    			break;
    		}
    	
    	
    	parahIndex = parahIndex<0?0:parahIndex;
    	int front = ParahList.parahIndex[parahIndex];
    	int rear  = parahIndex>=29?(NUM_AWESOME_VIEWS):ParahList.parahIndex[parahIndex+1];
    	
    	readingProgressbar.setMax(rear-front);
    	readingProgressbar.setProgress(currentPage-front+1);
    }
    

	void checkRukuCount()
	{
		for(int i=0; i<114;i++)
			if(rukuIndex[i].length != totalRuku[i])
			{
				//Log.d("checkRuku()", "check FAILED on surah#"+(i+1)+ " total:"+totalRuku[i]+" Indices:"+rukuIndex[i].length);
				return;
			}
		//Log.d("checkRuku()", "check PASSED");
	}
    
    short[] surahEndAndStartPage={107, 222, 236, 256, 262, 268, 306, 313, 332, 360, 367, 377, 386, 397, 405, 412, 435, 441, 446, 453, 459, 468, 490, 496, 499, 507, 516, 521, 524, 529, 532, 535, 538, 546, 552, 554, 555, 565, 568, 570, 572, 574, 577,
    		581, 585, 588, 590, 591, 592, 593, 595, 596, 597, 598, 599, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611};    
    
    short[] ayatIndices = {0, 0, 0, 0, 5, 16, 24, 30, 38, 49, 58, 62, 70, 77, 84, 89, 94, 102, 106, 113, 120, 127, 135, 142, 147, 157, 165, 173, 179, 185, 188, 194, 198, 204, 212, 216, 220, 225, 230, 233, 236, 243, 248, 253, 257, 260, 265, 270, 275, 282, 283, 1, 9, 16, 23, 30, 38, 46, 54, 63, 70, 78, 84, 92, 101, 109, 116, 122, 133, 141, 148, 154, 158, 166, 174, 181, 187, 194, 1, 6, 11, 13, 19, 24, 29, 35, 42, 47, 55, 61, 69, 76, 81, 88, 92, 96, 102, 106, 114, 122, 128,
    		135, 141, 148, 155, 163, 171, 176, 4, 6, 10, 14, 18, 24, 32, 37, 42, 46, 51, 57, 64, 69, 76, 83, 90, 96, 104, 109, 114, 1, 9, 19, 27, 35, 44, 52, 59, 68, 74, 82, 91, 95, 102, 111, 119, 125, 131, 138, 143, 147, 152, 158, 166, 12, 23, 31, 38, 44, 52, 58, 68, 74, 82, 88, 96, 105, 122, 131, 138, 144, 150, 156, 160, 164, 171, 178, 188, 196, 1, 9, 17, 26, 34, 41, 46, 53, 62, 70, 1, 7, 14, 21, 27, 32, 37, 42, 48, 55, 62, 69, 73,
    		80, 87, 94, 100, 107, 112, 118, 123, 1, 7, 15, 21, 26, 34, 42, 53, 62, 71, 79, 89, 98, 107, 6, 13, 20, 29, 38, 46, 54, 63, 73, 82, 89, 98, 109, 118, 5, 15, 23, 31, 38, 44, 53, 64, 70, 79, 87, 96, 103, 1, 6, 14, 18, 27, 34, 42, 6, 11, 21, 27, 36, 44, 2, 19, 34, 53, 73, 91, 7, 15, 27, 35, 43, 55, 65, 73, 80, 88, 94, 103, 112, 119, 1, 8, 18, 30, 41, 52, 61, 70, 81, 93, 101, 1, 12, 19, 23, 30, 39, 48,
    		55, 63, 75, 84, 97, 110, 12, 26, 39, 52, 64, 76, 96, 13, 39, 52, 66, 77, 87, 98, 114, 127, 1, 12, 25, 36, 46, 58, 73, 82, 91, 103, 2, 7, 16, 24, 31, 38, 47, 56, 65, 73, 1, 18, 28, 43, 59, 75, 90, 105, 1, 8, 19, 27, 32, 37, 44, 54, 59, 62, 3, 12, 21, 33, 45, 58, 69, 5, 24, 44, 65, 91, 118, 143, 167, 190, 216, 6, 17, 27, 40, 47, 60, 67, 80, 89, 6, 14, 21, 29, 36, 44, 51, 60, 71, 78, 85, 6, 14, 22,
    		29, 37, 45, 53, 64, 6, 16, 25, 33, 42, 50, 59, 11, 19, 28, 1, 11, 21, 1, 7, 16, 23, 31, 36, 44, 51, 55, 63, 1, 8, 15, 23, 33, 41, 49, 4, 12, 19, 30, 38, 44, 10, 22, 38, 53, 71, 4, 30, 57, 83, 109, 134, 161, 4, 19, 28, 44, 63, 83, 6, 11, 22, 32, 41, 48, 57, 68, 75, 8, 17, 26, 33, 41, 50, 59, 67, 78, 1, 12, 21, 29, 39, 47, 1, 11, 16, 23, 32, 44, 52, 12, 24, 36, 50, 64, 80, 5, 27, 40, 7,
    		18, 27, 1, 9, 16, 23, 30, 2, 12, 19, 30, 1, 10, 16, 24, 29, 5, 12, 1, 16, 35, 7, 31, 51, 15, 32, 1, 26, 40, 4, 25, 45, 14, 39, 67, 17, 52, 77, 4, 11, 18, 25, 1, 7, 12, 21, 4, 10, 17, 1, 5, 11, 5, 14, 7, 4, 1, 9, 1, 6, 1, 7, 1, 11, 23, 7, 32, 49, 13, 35, 5, 32, 3, 19, 2, 26, 28, 16, 1, 31, 50, 15, 1, 15, 31, 26, 1, 28, 9, 39, 21, 8, 6, 11, 34, 22, 14, 1, 8, 10,
    		1, 6, 19, 1, 18, 7, 9, 1, 1, 3, 1
    };
    
    short[] totalAyaat = {7, 286, 200, 176, 120, 165, 206, 75, 129, 109, 
    					123, 111, 43, 52, 99, 128, 111, 110, 98, 135, 
    					112, 78, 118, 64, 77, 227, 93, 88, 69, 60, 
			    		34, 30, 73, 54, 45, 83, 182, 88, 75, 85, 
			    		54, 53, 89, 59, 37, 35, 38, 29, 18, 45, 
			    		60, 49, 62, 55, 78, 96, 29, 22, 24, 13, 
			    		14, 11, 11, 18, 12, 12, 30, 52, 52, 44, 
			    		28, 28, 20, 56, 40, 31, 50, 40, 46, 42, 
			    		29, 19, 36, 25, 22, 17, 19, 26, 30, 20, 
			    		15, 21, 11, 8, 8, 19, 5, 8, 8, 11, 
			    		11, 8, 3, 9, 5, 4, 7, 3, 6, 3, 
			    		5, 4, 5, 6};

    byte[] totalRuku = {1,40, 20,24,16,20,24,10,16,11,
						10,12,6,7,6, 16,12,12,6,8,
						7,10,6,9,6, 11,7,9,7,6,
						4,3,9,6,5,   5,5,5,8,9,
						6,5,7,3,4,   4,4,4,2,3,
						3,2,3,3,3,   3,4,3,3,2,
						2,2,2,2,2,   2,2,2,2,2,
						2,2,2,2,2,   2,2,2,2,1,
						1,1,1,1,1,   1,1,1,1,1,
						1,1,1,1,1,   1,1,1,1,1,
						1,1,1,1,1,   1,1,1,1,1,
						1,1,1,1};

short[][] rukuIndex = { {1},{1,8,21,30,40,47,60,62,72,83,87,97,104,113,122,130,142,148,153,164,168,177,183,189,197,211,217,222,229,232,236,243,249,254,258,261,267,274,282,284},
						{1,10,21,31,42,55,64,72,81,92,102,110,121,130,144,149,156,172,181,190},{1,11,15,23,26,34,43,51,60,71,77,88,92,97,101,105,113,116,127,135,142,153,163,172},
						{1,6,12,20,27,35,44,51,57,67,78,87,94,101,109,116},{1,11,21,31,42,51,56,61,71,83,91,95,101,111,122,130,141,145,151,155},
						{1,11,26,32,40,48,54,59,65,73,85,94,100,109,127,130,142,148,152,158,163,172,182,189},{1,11,20,29,38,45,49,59,65,70},{1,7,17,25,30,38,43,60,67,73,81,90,100,111,119,123},
						{1,11,21,32,41,54,61,71,83,93,104},{1,9,25,36,50,61,69,84,96,110},{1,7,21,30,36,43,50,58,69,80,94,105},{1,8,19,27,32,38},{1,7,13,22,28,35,42},{1,16,26,45,61,80},
						{1,10,22,26,35,41,51,61,66,71,77,84,90,101,111,120},{1,11,23,31,41,53,61,71,78,85,94,101},{1,13,18,23,32,45,50,54,60,71,83,102},{1,16,41,51,66,83},{1,25,55,77,90,105,116,129},{1,11,30,42,51,75,94},
						{1,11,23,26,34,39,49,58,65,73},{1,23,33,51,78,93},{1,11,21,27,35,41,51,58,62},{1,10,21,35,45,61},{1,10,34,52,69,105,123,141,160,176,192},{1,15,32,45,59,67,83},
						{1,14,22,29,43,51,61,76,83},{1,14,23,31,45,52,64},{1,11,20,28,41,54},{1,12,20,31},{1,12,23},{1,9,21,28,35,41,53,59,69},{1,10,22,31,37,46},{1,8,15,27,38},{1,13,33,51,68},
						{1,22,75,114,134},{1,15,27,41,65},{1,10,22,32,42,53,64,71},{1,10,21,28,38,51,61,69,79},{1,9,19,26,33,45},{1,10,20,30,44},{1,16,26,36,46,57,68},{1,30,43},{1,12,22,27},{1,11,21,27},
						{1,12,20,29},{1,11,18,27},{1,11},{1,16,30},{1,24,27},{1,29},{1,26,33},{1,23,41},{1,26,46},{1,39,75},{1,11,20,26},{1,7,14},{1,11,18},{1,7},{1,10},{1,9},{1,9},{1,11},{1,8},{1,8},{1,15},
						{1,34},{1,38},{1,36},{1,21},{1,20},{1,20},{1,32},{1,31},{1,23},{1,41},{1,31},{1,27},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},
						{1},{1},{1},{1},{1},{1},{1},{1},{1},{1},{1}
					};

abstract class LongTouchActionListener implements OnTouchListener {

    /**
     * Implement these methods in classes that extend this
     */
    public abstract void onClick(View v);
    public abstract void onLongTouchAction(View v);

    /**
     * The time before we count the current touch as
     * a long touch
     */
    public static final long LONG_TOUCH_TIME = 500;

    /**
     * The interval before calling another action when the
     * users finger is held down
         */
    public static final long LONG_TOUCH_ACTION_INTERVAL = 100;

    /**
     * The time the user first put their finger down
     */
    private long mTouchDownTime;

    /**
     * The coordinates of the first touch
     */
    private float mTouchDownX;
    private float mTouchDownY;

    /**
     * The amount the users finger has to move in DIPs
     * before we cancel the touch event
     */
    public static final int TOUCH_MOVE_LIMIT_DP = 50;

    /**
     * TOUCH_MOVE_LIMIT_DP converted to pixels, and squared
     */
    private float mTouchMoveLimitPxSq;

    /**
     * Is the current touch event a long touch event
         */
    private boolean mIsLongTouch;

    /**
     * Is the current touch event a simple quick tap (click)
     */
    private boolean mIsClick;

    /**
     * Handlers to post UI events
     */
    private LongTouchHandler mHandler;

    /**
     * Reference to the long-touched view
     */
    private View mLongTouchView;

    /**
     * Constructor
     */

    public LongTouchActionListener(Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        mTouchMoveLimitPxSq = scale*scale*TOUCH_MOVE_LIMIT_DP*TOUCH_MOVE_LIMIT_DP;

        mHandler = new LongTouchHandler();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        final int action = event.getAction();

        switch (action) {

        case MotionEvent.ACTION_DOWN:
            // down event
            mIsLongTouch = false;
            mIsClick = true;

            mTouchDownX = event.getX();
            mTouchDownY = event.getY();
            mTouchDownTime = event.getEventTime();

            mLongTouchView = view;

            // post a runnable
            mHandler.sendEmptyMessageDelayed(LongTouchHandler.MESSAGE_LONG_TOUCH_WAIT, LONG_TOUCH_TIME);
            break;

        case MotionEvent.ACTION_MOVE:
            // check to see if the user has moved their
            // finger too far
            if (mIsClick || mIsLongTouch) {
                final float xDist = (event.getX() - mTouchDownX);
                final float yDist = (event.getY() - mTouchDownY);
                final float distanceSq = (xDist*xDist) + (yDist*yDist);

                if (distanceSq > mTouchMoveLimitPxSq) {
                    // cancel the current operation
                    mHandler.removeMessages(LongTouchHandler.MESSAGE_LONG_TOUCH_WAIT);
                    mHandler.removeMessages(LongTouchHandler.MESSAGE_LONG_TOUCH_ACTION);

                    mIsClick = false;
                    mIsLongTouch = false;
                }
            }
            break;

        case MotionEvent.ACTION_CANCEL:
            mIsClick = false;
        case MotionEvent.ACTION_UP:
            // cancel any message
            mHandler.removeMessages(LongTouchHandler.MESSAGE_LONG_TOUCH_WAIT);
            mHandler.removeMessages(LongTouchHandler.MESSAGE_LONG_TOUCH_ACTION);

            long elapsedTime = event.getEventTime() - mTouchDownTime;
            if (mIsClick && elapsedTime < LONG_TOUCH_TIME) {
                onClick(view);
            }
            break;

        }

        // we did not consume the event, pass it on
        // to the button
        return false; 
    }

    /**
     * Handler to run actions on UI thread
     */
    private class LongTouchHandler extends Handler {
        public static final int MESSAGE_LONG_TOUCH_WAIT = 1;
        public static final int MESSAGE_LONG_TOUCH_ACTION = 2;
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_LONG_TOUCH_WAIT:
                    mIsLongTouch = true;
                    mIsClick = false;

                    // flow into next case
                case MESSAGE_LONG_TOUCH_ACTION:
                    if (!mIsLongTouch) return;

                    onLongTouchAction(mLongTouchView); // call users function

                    // wait for a bit then update
                    takeNapThenUpdate(); 

                    break;
            }
        }

        private void takeNapThenUpdate() {
            sendEmptyMessageDelayed(MESSAGE_LONG_TOUCH_ACTION, LONG_TOUCH_ACTION_INTERVAL);
        }
    };
};

}