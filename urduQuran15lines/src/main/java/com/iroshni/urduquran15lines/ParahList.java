package com.iroshni.urduquran15lines;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class ParahList extends Fragment implements OnItemClickListener {
	ArrayAdapter<String> adapter = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    

    @Override
    public View onCreateView(LayoutInflater inflater,
       final ViewGroup container, Bundle savedInstanceState) {

    	//setContentView(R.layout.parah_numbers);
		adapter = new ParahCustomAdapter<String>(inflater.getContext(),R.layout.parah_list_item, Arrays.asList(parahNames_array));
		ListView listView = new ListView(inflater.getContext());
		listView.setOnItemClickListener(this);
		listView.setAdapter(adapter);
		listView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		LinearLayout ll = new LinearLayout(inflater.getContext());
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		ll.setGravity(Gravity.CENTER);
		ll.setFocusable(true);
		ll.setFocusableInTouchMode(true);
    	
		ll.addView(listView);
		AwesomePagerActivity.index_activity_response = -1;
		//View view = inflater.inflate(R.layout.parah_numbers, container, false);
	    //return view;
		return ll;
    }
    
    @Override 
	public void onResume()
	{
		super.onResume();
		AwesomePagerActivity.index_activity_response = -1;
	}
    
	public void onItemClick(AdapterView arg0, View v, int position, long arg3) {
	// TODO Auto-generated method stub
	//Toast.makeText(this, "u clicked " + surahName_array[position] ,Toast.LENGTH_LONG).show();
	AwesomePagerActivity.index_activity_response= parahIndex[position];
	getActivity().finish();
		//awesomePager.setCurrentItem(surahIndex[position]);
		
	}    

    static String parahNumber_array[] = {"01","02","03","04","05","06","07","08","09","10", "11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"};
    static short[] parahIndex = { 2, 23, 43, 63, 83, 103, 123, 143, 163, 183, 203, 223, 243, 263, 283, 303, 323, 343, 363, 383, 403, 423, 443, 463, 483, 503, 523, 543, 563, 587 };
	static String parahNames_array[] = {"الٓمّٓ", "سَيَقُولُ", "تِلْكَ الرُّسُلُ", "لَنْ تَنَالُوا", "وَالْمُحْصَنٰتُ", "لَا يُحِبُّ اللَّهُ", "وَاِذَا سَمِعُوا", "وَلَوْ اَنَّنَا", "قَالَ الْمَلَاُ", "وَاعْلَمُوْۤا", "يَعْتَذِرُونَ", "وَمَا مِنْ دَآبَّةٍ", "وَمَاۤ اُبَرِّیُ", "رُبَمَا", "سُبْحٰنَ الَّذِیْۤ", "قَالَ اَلَمْ", "اِقْتَرَبَ لِلنَّاسِ", "قَدْ اَفْلَحَ", "وَقَالَ الَّذِيْنَ", "اَمَّنْ خَلَقَ", "اُتْلُ مَا اُوْحِیَ", "وَمَنْ يَّقْنُتْ", "وَمَا لِیَ", "فَمَنْ اَظْلَمُ", "اِلَيْهِ يُرَدُّ", "حٰمٓ", "قَالَ فَمَا خَطْبُكُمْ", "قَدْ سَمِعَ اللهُ", "تَبٰرَكَ الَّذِیْ", "عَمَّ يَتَسَاءَلُونَ", };

    public void myOnClick(View v)
    {
    	int pos = Integer.parseInt((String) ((Button)v).getText());
    	AwesomePagerActivity.index_activity_response= parahIndex[pos-1];
    	getActivity().finish();
    }
 
}

class ParahCustomAdapter<T> extends ArrayAdapter<T> implements Filterable {

	private List<T> mObjects;
	List<T> mOrignalObjects;
	Context context;
	Typeface custom_font;

	public ParahCustomAdapter(Context context, int textViewResourceId, List<T> objects) {
		super(context, textViewResourceId, objects);

		mOrignalObjects = objects;
		mObjects = objects;
		this.context = context;
		custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/PDMS_Saleem_QuranFont.ttf");
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		// A ViewHolder keeps references to children views to avoid unnecessary calls
		// to findViewById() on each row.
		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.parah_list_item, parent, false);
			holder = new ViewHolder();
            holder.textViewArabic = (TextView) convertView.findViewById(R.id.parah_name_urdu);
			holder.textViewNum = (TextView) convertView.findViewById(R.id.parah_num);
			holder.textViewArabic.setTypeface(custom_font);
			// Bind the data efficiently with the holder.
			convertView.setTag(holder);
		} else {
			// Get the ViewHolder back to get fast access to the TextViews
			holder = (ViewHolder) convertView.getTag();
		}
		holder.textViewArabic.setText(mObjects.get(position).toString());
		holder.textViewNum.setText(ParahList.parahNumber_array[position]);

		return convertView;
	}

	static class ViewHolder {
		TextView textViewArabic;
		TextView textViewNum;
	}

	@Override
	public int getCount() {
		//return mObjects.size();
		return mObjects.size();
	}

	@Override
	public T getItem(int position) {
		return mObjects.get(position);
	}
}