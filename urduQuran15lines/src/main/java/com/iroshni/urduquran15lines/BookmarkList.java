package com.iroshni.urduquran15lines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class BookmarkList extends ActionBarListActivity{


	ListView listView;
	MyCustomAdapter dataAdapter = null;
	private TouchInterceptor mList;
	private TouchInterceptor.DropListener mDropListener;
	ArrayList<BookmarkHolder> bookmarkList;
	int currentFolder;
	Editor preferenceEditor;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if( AwesomePagerActivity.appOrientation != AwesomePagerActivity.ORIENTATION_AUTO)
        	setRequestedOrientation(AwesomePagerActivity.appOrientation==AwesomePagerActivity.ORIENTATION_LANDSCAPE?ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        if(AwesomePagerActivity.appBrightnessValue > 0)
        {
        	WindowManager.LayoutParams layout_for_brightness = getWindow().getAttributes();
			layout_for_brightness.screenBrightness = AwesomePagerActivity.appBrightnessValue;
			getWindow().setAttributes(layout_for_brightness);
        }
        
        setContentView(R.layout.bookmark_list);
        setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        
        setTitle("Bookmarks");
        currentFolder = getPreferences(Context.MODE_PRIVATE).getInt("currentFolder", -1);
        List<BookmarkObject> list;
		
        if(currentFolder<0)
			list = AwesomePagerActivity.bookmarks;
		else
		{
			list = AwesomePagerActivity.bookmarks.get(currentFolder).list;
			setTitle("Bookmarks: "+AwesomePagerActivity.bookmarks.get(currentFolder).desc);
		}
        
        
		//setCacheColorHint(0);
		bookmarkList = new ArrayList<BookmarkHolder>();
		
		for(int i=0; i<list.size(); i++)
		{
			BookmarkObject o = list.get(i);
			bookmarkList.add(new BookmarkHolder(o.desc, o.page, o.isFolder));
		}
		
		dataAdapter = new MyCustomAdapter(this,R.layout.bookmark_list_item, bookmarkList);
		setListAdapter(dataAdapter);
		mDropListener = new TouchInterceptor.DropListener() {
				public void drop(int from, int to) {					
					//Assuming that item is moved up the list
					int direction = -1;
					int loop_start = from;
					int loop_end = to;
			        List<BookmarkObject> list;
					
			        if(currentFolder<0)
						list = AwesomePagerActivity.bookmarks;
					else
						list = AwesomePagerActivity.bookmarks.get(currentFolder).list;
	
					//For instance where the item is dragged down the list
					if(from < to) 
						direction = 1;
	
					//BookmarkHolder target = dataAdapter.getItem(from);
					BookmarkHolder t = bookmarkList.get(from);
					BookmarkObject fromObj = list.get(from);
					for(int i=loop_start;i!=loop_end;i=i+direction)
					{
						bookmarkList.set(i, bookmarkList.get(i+direction));
						list.set(i, list.get(i+direction));
					}
					//dataAdapter.setItem(to, target);
					bookmarkList.set(to, t);
					list.set(to, fromObj);
	
					((BaseAdapter) mList.getAdapter()).notifyDataSetChanged();
					dataAdapter.clearSelection();
				}

			};
		
		mList = (TouchInterceptor) getListView();
		mList.setDropListener(mDropListener);
		registerForContextMenu(mList);
		AwesomePagerActivity.index_activity_response = -1;
		AwesomePagerActivity.saveSettings = false;
		preferenceEditor = getPreferences(Context.MODE_PRIVATE).edit();
		if(currentFolder<0)
			;
		else
			((ImageButton) findViewById(R.id.btn_add)).setEnabled(false);
				
		
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
        	
        	if(this.currentFolder < 0)
        		finish();
        	else
        	{
        		dataAdapter.clear();
        		for(int i=0; i<AwesomePagerActivity.bookmarks.size(); i++)
        		{
        			BookmarkObject o = AwesomePagerActivity.bookmarks.get(i);
        			bookmarkList.add(new BookmarkHolder(o.desc, o.page, o.isFolder));
        		}
        		dataAdapter.notifyDataSetChanged();
		        dataAdapter.clearSelection();
        		
        		((ImageButton) findViewById(R.id.btn_add)).setEnabled(true);
        		setTitle("Bookmarks");
        		currentFolder = -1;
        	}
        }

        return super.onOptionsItemSelected(item);
    }

	
	//ActionListner for buttons
	public void performAction(View v)
	{
		int id = v.getId();
        switch(id) {
        
        	case R.id.btn_add:
        		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        		builder.setTitle("Name new folder:");
        		builder.setIcon(R.drawable.ic_menu_archive);

        		// Set up the input
        		final EditText input = new EditText(this);
        		builder.setView(input);

        		// Set up the buttons
        		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
        		    @Override
        		    public void onClick(DialogInterface dialog, int which) {
        		        String m_Text = input.getText().toString().replace("\n", " ").trim();   
        		        
                		for(int i=0; i<AwesomePagerActivity.bookmarks.size(); i++)
                		{
                			BookmarkObject o = AwesomePagerActivity.bookmarks.get(i);
                			if(o.isFolder && o.desc.equals(m_Text))
                			{
                				Toast.makeText(getApplicationContext(), "Folder with this name already exists!",Toast.LENGTH_LONG).show();
                				return;
                			}
                		}
                				
        		        bookmarkList.add(0, new BookmarkHolder(m_Text, -1, true));
        		        dataAdapter.notifyDataSetChanged();
        		        dataAdapter.clearSelection();        		        
        		        AwesomePagerActivity.bookmarks.add(0, new BookmarkObject(m_Text, 0, true));
        		    }
        		});
        		builder.setNegativeButton("Cancel", null);
        		builder.show();
        		break;
        		
//        	case R.id.btn_back:
//        		dataAdapter.clear();
//        		for(int i=0; i<AwesomePagerActivity.bookmarks.size(); i++)
//        		{
//        			BookmarkObject o = AwesomePagerActivity.bookmarks.get(i);
//        			bookmarkList.add(new BookmarkHolder(o.desc, o.page, o.isFolder));
//        		}
//        		dataAdapter.notifyDataSetChanged();
//		        dataAdapter.clearSelection();
//        		
//        		((ImageButton) findViewById(R.id.btn_add)).setEnabled(true);
//        		((ImageButton) findViewById(R.id.btn_back)).setEnabled(false);
//        		setTitle("Bookmarks");
//        		currentFolder = -1;
//        		break;

        	case R.id.btn_delete:
        		
        		if(dataAdapter.getCurrentCheckedPosition().iterator().hasNext() == false)
        		{
        			Toast.makeText(getApplicationContext(), "First select bookmarks to delete",Toast.LENGTH_LONG).show();
        			return;
        		}

        	    new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_menu_delete)
                .setTitle("Delete")
                .setMessage("Are you sure to delete selected bookmarks?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
	            {
	                @Override
	                public void onClick(DialogInterface dialog, int which) {
	            		Set<Integer> indices = dataAdapter.getCurrentCheckedPosition();
	            		Iterator<Integer> iterator = indices.iterator();
	            		BookmarkObject objs[] = new BookmarkObject[indices.size()]; 
	            		BookmarkHolder holders[] = new BookmarkHolder[indices.size()]; 
	            		List<BookmarkObject> list;
	            		
	            		if(currentFolder<0)
	            			list = AwesomePagerActivity.bookmarks;
	            		else
	            			list = AwesomePagerActivity.bookmarks.get(currentFolder).list;
	            		
	            		for(int x=0; x<objs.length; x++)
	            		{
	            			int i = iterator.next().intValue();
	            			objs[x] = list.get(i);
	            			holders[x] = bookmarkList.get(i);
	            		}

	            		for(int x=0; x<objs.length; x++)
	            		{
	            			bookmarkList.remove(holders[x]);
	            			list.remove(objs[x]);
	            		}
	            		dataAdapter.notifyDataSetChanged();
	            		dataAdapter.clearSelection();
	                }
	
	            })
	            .setNegativeButton("No", null)
	            .show();        		
        		break;
        		
        	case R.id.btn_move:
        		
        		Iterator<Integer> it = dataAdapter.getCurrentCheckedPosition().iterator();
        		if(it.hasNext() == false)
        		{
        			Toast.makeText(getApplicationContext(), "First select bookmarks to move",Toast.LENGTH_LONG).show();
        			return;
        		}
        		else if(currentFolder < 0)
        		{
        			while(it.hasNext())
        			{
        				if(bookmarkList.get(it.next().intValue()).isFolder)
        				{
        					Toast.makeText(getApplicationContext(), "Folders cannot be moved",Toast.LENGTH_LONG).show();
        					return;
        				}
        			}
        		}
        		
        		AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
                builderSingle.setTitle("Destination folder");
                builderSingle.setIcon(R.drawable.ic_menu_archive);
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        		
                if(currentFolder >= 0)
        			arrayAdapter.add("<root>");

        		for(int i=0; i<AwesomePagerActivity.bookmarks.size(); i++)
        		{
        			BookmarkObject o = AwesomePagerActivity.bookmarks.get(i);
        			if(o.isFolder && i != currentFolder)
        				arrayAdapter.add(o.desc);
        		}
        		
                builderSingle.setNegativeButton("Cancel", null);

                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String destFolder = arrayAdapter.getItem(which);                       		
                                Set<Integer> indices = dataAdapter.getCurrentCheckedPosition();
                        		BookmarkObject objs[] = new BookmarkObject[indices.size()]; 
                        		BookmarkHolder holders[] = new BookmarkHolder[indices.size()]; 
                        		Iterator<Integer> iterator = indices.iterator();
                        		List<BookmarkObject> srcList, destList=null;
                        		
                        		if(currentFolder < 0)
                        			srcList = AwesomePagerActivity.bookmarks;
                        		else
                        			srcList = AwesomePagerActivity.bookmarks.get(currentFolder).list;
                        		
                        		if(destFolder.equals("<root>"))
                        			destList = AwesomePagerActivity.bookmarks;
                        		else
                            		for(int i=0; i<AwesomePagerActivity.bookmarks.size(); i++)
                            		{
                            			BookmarkObject o = AwesomePagerActivity.bookmarks.get(i);
                            			if(o.isFolder && o.desc.equals(destFolder))
                            			{
                            				destList = o.list;
                            				break;
                            			}
                            		}                        			
                        			
                        		for(int x=0; x<objs.length; x++)
                        		{
                        			int i = iterator.next().intValue();
                        			objs[x] = srcList.get(i);
                        			holders[x] = bookmarkList.get(i);
                        		}

                        		for(int x=0; x<objs.length; x++)
                        		{
                        			bookmarkList.remove(holders[x]);
                        			destList.add(objs[x]);
                        			srcList.remove(objs[x]);
                        		}
                        		dataAdapter.notifyDataSetChanged();
                        		dataAdapter.clearSelection();                                
                            }
                        });
                if( arrayAdapter.isEmpty() )
                	Toast.makeText(getApplicationContext(), "Firt create a folder to move selected bookmarks",Toast.LENGTH_LONG).show();
                else
                	builderSingle.show();        		
        		break;
        	
        	case R.id.btn_share:        		
        		Iterator<Integer> iterator = dataAdapter.getCurrentCheckedPosition().iterator();
        		String shareText = "";
        		
        		if(iterator.hasNext() == false)
        		{
        			Toast.makeText(getApplicationContext(), "First select bookmarks to share",Toast.LENGTH_LONG).show();
        			return;
        		}
        		shareText = prepareShareText(iterator);
        		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Bookmarks");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareText);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));        		
        		break;

        	case R.id.btn_selectall:        		
                dataAdapter.selectAll();
        		break;

        }

	}
	
	private String prepareShareText(Iterator<Integer> iterator)
	{
		String shareText = "";
		List<BookmarkObject> list;
		BookmarkObject obj;
		
		if(currentFolder<0)
			list = AwesomePagerActivity.bookmarks;
		else
			list = AwesomePagerActivity.bookmarks.get(currentFolder).list;
		
		while(iterator.hasNext())
		{
			int i = iterator.next().intValue();
			obj = list.get(i);
			if(obj.isFolder)
			{
				for(int j=0; j<obj.list.size(); j++)
				{
					shareText += obj.list.get(j).desc+"\n";
					shareText += "[Surah] "+AwesomePagerActivity.getSurahName(obj.list.get(j).page).replace(':', '.')+"\n";
					shareText += "[Page] "+obj.list.get(j).page+"\n\n";
				}
			}
			else
			{
				shareText += obj.desc+"\n";
				shareText += "[Surah] "+AwesomePagerActivity.getSurahName(obj.page).replace(':', '.')+"\n";
				shareText += "[Page] "+obj.page+"\n\n";
			}
		}
		
		shareText += "\nThe given page numbers are referenced from Android App \"Urdu Quran (15 lines per page)\" [https://play.google.com/store/apps/details?id=com.iroshni.urduquran15lines]";
		
		return shareText;
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		BookmarkObject obj;
		
		if(currentFolder<0)
			obj = AwesomePagerActivity.bookmarks.get(position);
		else
			obj = AwesomePagerActivity.bookmarks.get(currentFolder).list.get(position);
		
		if(obj.isFolder)
		{
			bookmarkList.clear();
			for(int i=0; i<obj.list.size(); i++)
			{
    			BookmarkObject o = obj.list.get(i);
    			bookmarkList.add(new BookmarkHolder(o.desc, o.page, o.isFolder));				
			}
			dataAdapter.notifyDataSetChanged();
			((ImageButton) findViewById(R.id.btn_add)).setEnabled(false);		
//			((ImageButton) findViewById(R.id.btn_back)).setEnabled(true);
			currentFolder = position;
	        dataAdapter.clearSelection();
	        setTitle("Bookmarks: "+obj.desc);
		}
		else
		{
			AwesomePagerActivity.index_activity_response = obj.page;			
			finish();
		}
	}
	
	@Override 
	public void onResume()
	{
		super.onResume();
		AwesomePagerActivity.index_activity_response = -1;
	}

	@Override 
	public void onPause()
	{
		preferenceEditor.putInt("currentFolder", currentFolder);
		AwesomePagerActivity.saveSettings = true;
		preferenceEditor.commit();
		super.onPause();
	}
	
	
private class BookmarkHolder{
	String bookmark_desc;
	TextView desc_view;
	int position;
	int page;
	boolean isFolder;
	View convertView;
	public BookmarkHolder(String s, int p, boolean f){
		bookmark_desc = s;
		page = p;
		isFolder = f;
	}	
}

private class MyCustomAdapter extends ArrayAdapter<BookmarkHolder> {
	 
	  private ArrayList<BookmarkHolder> countryList;
	  private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
	 
	  public MyCustomAdapter(Context context, int textViewResourceId, ArrayList<BookmarkHolder> countryList) {
		  super(context, textViewResourceId, countryList);
		  this.countryList = new ArrayList<BookmarkHolder>();
		  this.countryList.addAll(countryList);
	  }
	  
	  public void setItem(int index, BookmarkHolder h){
		  countryList.set(index, h);
	  }
	 
	  public Set<Integer> getCurrentCheckedPosition() {
          return mSelection.keySet();
      }
      
	  public void clearSelection() {
          mSelection = new HashMap<Integer, Boolean>();
      }	 

	  public void selectAll(){
		  
		  for(int x=0; x<getCount(); x++)
		  {
			  ViewGroup g = (ViewGroup) getItem(x).convertView;			  
			  int count = g.getChildCount();
			  for(int i=0; i<count; i++)
				  if(g.getChildAt(i) instanceof CheckBox)
				  {
					  ((CheckBox)g.getChildAt(i)).setChecked(true);
					  break;
				  }
			  
			  getItem(x).desc_view.setTypeface(null, Typeface.BOLD);
			  getItem(x).convertView.setBackgroundColor(Color.argb(130,9, 150, 190));
			  mSelection.put(x, true);
		  }
	  }
	  	  
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	 
	 
		LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = vi.inflate(R.layout.bookmark_list_item, null);
		
		   
		TextView desc = (TextView) convertView.findViewById(R.id.code);
		CheckBox box = (CheckBox) convertView.findViewById(R.id.bookmark_delete);		
	    box.setOnClickListener(listner);
	    BookmarkHolder bH = dataAdapter.getItem(position);
		if(bH.isFolder)
		{
			desc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu_archive, 0, 0, 0);	    
			desc.setGravity(Gravity.CENTER);
		}
		desc.setText(bH.bookmark_desc);
	    bH.desc_view = desc;
	    bH.position = position;
	    bH.convertView = convertView;
	    box.setTag(bH);		   
	   
	    return convertView;
	 
	  }
	  
	  View.OnClickListener listner = new View.OnClickListener() {  
		   public void onClick(View v) {  
			   CheckBox cb = (CheckBox) v ;  
			   BookmarkHolder bH = (BookmarkHolder) cb.getTag();
			   int position = bH.position;
			   
			   if(cb.isChecked())
			   {
				   bH.desc_view.setTypeface(null, Typeface.BOLD);
				   bH.convertView.setBackgroundColor(Color.argb(130,9, 150, 190));
				   mSelection.put(position, true);
			   }
			   else
			   {
				   bH.desc_view.setTypeface(null, Typeface.NORMAL);
				   bH.convertView.setBackgroundColor(0);
				   mSelection.remove(position);
			   }
		   }  
	   };
	 }
	 
	
} //class

class BookmarkObject{
	
	String desc;
	int page;
	boolean isFolder;
	LinkedList<BookmarkObject> list;
	
	public BookmarkObject(String d, int p, boolean f)
	{
		desc = d;
		page = p;
		isFolder = f;
		list = new LinkedList<BookmarkObject>();
	}
	
	public void add(String d, int p)
	{
		list.add(new BookmarkObject(d, p, false));	
	}
	
}//class

