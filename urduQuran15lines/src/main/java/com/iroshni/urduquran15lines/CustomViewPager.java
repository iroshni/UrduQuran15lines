package com.iroshni.urduquran15lines;

import java.util.LinkedList;
import android.content.Context; 
import android.support.v4.view.ViewPager; 
import android.util.AttributeSet; 
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.webkit.WebView;

public class CustomViewPager extends ViewPager {

private volatile WebView mCurrentPageWebView_; //custom webview

public CustomViewPager(Context context, AttributeSet attrs) {
    super(context, attrs);
}

@Override
public boolean onInterceptTouchEvent(MotionEvent event) {
   
      //  Log.d("", "CustomViewPager - onInterceptTouchEvent");
  

    // if view zoomed out (view starts at 33.12... scale level) ... allow
    // zoom within webview, otherwise disallow (allow viewpager to change
    // view)
    if (mCurrentPageWebView_ != null && (mCurrentPageWebView_.getScale() * 100) > 100) {
        //Log.d("", "CustomViewPager - intrcepted: " + String.valueOf((mCurrentPageWebView_.getScale() *100)));
        this.requestDisallowInterceptTouchEvent(true);
        //mCurrentPageWebView_.loadUrl("www.google.com");
    }
    else {
        //if (mCurrentPageWebView_ != null) Log.d("", "CustomViewPager - not intrcepted: " + String.valueOf(mCurrentPageWebView_.getScale() * 100));
        
        this.requestDisallowInterceptTouchEvent(false);
        
    }

    return super.onInterceptTouchEvent(event);
}

public WebView getCurrentPageWebView() {
    return mCurrentPageWebView_;
}

public void setCurrentPageWebView(WebView currentPageWebView) {
    mCurrentPageWebView_ = currentPageWebView;
}

} //class


/****************************************************************************************/

//Auxilary class
class ThreeElementStorage
{
	private LinkedList<Integer> indices;
	private LinkedList<MyWebView> views;
	
	public ThreeElementStorage()
	{
		indices = new LinkedList<Integer>();
		views   = new LinkedList<MyWebView>();
	}
	
	public void put(MyWebView v, int i)
	{
		//Log.d("ThreeElementStorage", "Put req for position "+i+" !!!");
		indices.addFirst(i);
		views.addFirst(v);
		
		if(indices.size()> 3 )
		{
			indices.removeLast();
			views.removeLast();
		}		
	}
	
	public void setZoomCtrl(boolean toggle)
	{
		for(int x=0; x<indices.size(); x++)
			views.get(x).showZoomButtons(toggle);
			//views.get(x).getSettings().setBuiltInZoomControls(toggle);		
	}
	
	public WebView get(int position)
	{
		int x=0;
		
		for(x=0; x<indices.size(); x++)
		{
			if(indices.get(x)==position)
				break;
		}
		
		if(x>=indices.size())
		{
			//Log.d("ThreeElementStorage", "!!! No webview could find for position "+position+" !!!");
			return null;
		}
		else
		{
			//if(x>=3) Log.d("ThreeElementStorage", "!!! List grows beyond 3 elements !!!");
			return views.get(x);
		}
	}

}//class

/*********************************************************************************************/

class MyWebView extends WebView
{

	boolean multiTouchZoom = true;
	boolean buttonsZoom = false;
	private GestureDetector gestureDetector;
	private GestureListener gestureListener;
	AwesomePagerActivity aPagerActivity;
	
	public MyWebView(Context context, AwesomePagerActivity awesomePagerActivity) {
		super(context);
		gestureListener = new GestureListener(awesomePagerActivity);
		gestureDetector = new GestureDetector(context, gestureListener);
		aPagerActivity = awesomePagerActivity;
	}
	
	public void showZoomButtons(boolean toggle)
	{
		buttonsZoom = toggle;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent ev)
	{
		 //Log.d("onTouchEvent","");
		   if (ev.getAction() == MotionEvent.ACTION_DOWN ||
		       ev.getAction() == MotionEvent.ACTION_POINTER_DOWN ||
		       ev.getAction() == MotionEvent.ACTION_POINTER_1_DOWN ||
		       ev.getAction() == MotionEvent.ACTION_POINTER_2_DOWN ||
		       ev.getAction() == MotionEvent.ACTION_POINTER_3_DOWN) 
		   {
		        if (multiTouchZoom && !buttonsZoom) {
		            if (ev.getPointerCount() > 1) {
		                getSettings().setBuiltInZoomControls(true);
	//	                getSettings().setSupportZoom(true);
		            } else {
		                getSettings().setBuiltInZoomControls(false);
//		                getSettings().setSupportZoom(false);
		            }
		        }
		        else
		        {
	                getSettings().setBuiltInZoomControls(true);
		        }
		    }
		   
		   if(AwesomePagerActivity.enableScreenTap)
		   {
			   //Log.d("Gesture", this.getScale()+ "@ ("+this.getScrollX()+", "+this.getScrollY()+" )");
			   gestureDetector.onTouchEvent(ev);
			   
		   }
		   
		   return super.onTouchEvent(ev);
		   
		   //if
		   
		/*	   return true;
		   else
			   return super.onTouchEvent(ev);
			   */
	}

	private class GestureListener extends GestureDetector.SimpleOnGestureListener {
	    private static final int SWIPE_MIN_DISTANCE = 120;
	    private static final int SWIPE_MAX_OFF_PATH = 250;
	    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	    AwesomePagerActivity aPagerActivity;
	    float adjX, adjY;
	    
	    public GestureListener(AwesomePagerActivity awesomePagerActivity)
	    {
	    	super();
	    	aPagerActivity = awesomePagerActivity;
	        DisplayMetrics metrics = getResources().getDisplayMetrics();
	        adjX = 2*metrics.xdpi/25.4f;
	        adjY = 2*metrics.xdpi/25.4f;
	    	
	    }
      @Override
      public boolean onSingleTapConfirmed (MotionEvent e){
      	 //Log.d("Single Tap", "Tapped");
    	  if(AwesomePagerActivity.tapGestureEnabled) 
    		  aPagerActivity.doNextPage();
    	  else
    	  {
              int page = aPagerActivity.getCurrentPage();
              WebView webview = aPagerActivity.liveWebView[page%3]; 
              float scaleX = webview.getScale()*aPagerActivity.svgScale[page%3][0];
              float scaleY = webview.getScale()*aPagerActivity.svgScale[page%3][1];
              float clickX = (float)((webview.getScrollX()+ e.getX() - adjX )/scaleX);
              float clickY = (float)((webview.getScrollY()+ e.getY() - adjY )/scaleY);
              //Log.e("", "single tap detected @ "+page+" "+e.getX()+", "+e.getY()+ ": ("+clickX+", "+clickY);
              
              if(aPagerActivity.highligts.get(page) == null)
            	  aPagerActivity.highligts.set(page, new LinkedList());
              
              aPagerActivity.highligts.get(page).add(new HighlightRect(clickX, clickY, 225));
              aPagerActivity.reloadPage(page);
              aPagerActivity.saveHighlights();
    	  }
    	  
          return true;
      }

      // event when double tap occurs
      @Override
      public boolean onDoubleTap(MotionEvent e) {
          /*float x = e.getX();
          float y = e.getY();
          Log.d("Double Tap", "Tapped at: (" + x + "," + y + ")");
          */
    	  if(AwesomePagerActivity.tapGestureEnabled) 
    		  aPagerActivity.doPreviousPage();
    	  else
    	  {
              int page = aPagerActivity.getCurrentPage();
              if(aPagerActivity.highligts.get(page) == null || aPagerActivity.highligts.get(page).size() <= 0)
            	  return true;
              
              WebView webview = aPagerActivity.liveWebView[page%3];
              float scaleX = webview.getScale()*aPagerActivity.svgScale[page%3][0];
              float scaleY = webview.getScale()*aPagerActivity.svgScale[page%3][1];
              float clickX = (float)((webview.getScrollX()+ e.getX() - adjX )/scaleX);
              float clickY = (float)((webview.getScrollY()+ e.getY() - adjY )/scaleY);
              
              boolean reloadRequired=false;
              int i=0;
              while(i<aPagerActivity.highligts.get(page).size())
              {
            	  if(aPagerActivity.highligts.get(page).get(i).contains(clickX, clickY))
            	  {
            		  reloadRequired = true;
            		  aPagerActivity.highligts.get(page).remove(i);
            	  }
            	  else
            		  i++;
              }
              
              if( reloadRequired)
              {
            	  aPagerActivity.reloadPage(page); 
              	  aPagerActivity.saveHighlights();
              }
              

    	  }

          return true;
      }
      
      @Override
      public boolean onDown(MotionEvent event) {
         return true;
      }
      
      @Override
      public void onLongPress(MotionEvent e) {
          //aPagerActivity.awesomePager.getAdapter().notifyDataSetChanged();
      }      
      
      @Override
      public boolean onFling(MotionEvent event1, MotionEvent event2, 
				float velocityX, float velocityY) {
			
			if(aPagerActivity.orientation == AwesomePagerActivity.ORIENTATION_LANDSCAPE || 
					AwesomePagerActivity.enableFullScreen == false)
				return false;
			
			//calculate the change in X position within the fling gesture
			float horizontalDiff = event2.getX() - event1.getX();
			//calculate the change in Y position within the fling gesture
			float verticalDiff = event2.getY() - event1.getY();
			
			//work out absolute values
			float absHDiff = Math.abs(horizontalDiff);
			float absVDiff = Math.abs(verticalDiff);
			float absVelocityX = Math.abs(velocityX);
			float absVelocityY = Math.abs(velocityY);
			
			//is horizontal difference greater and are values valid
			if(absHDiff>absVDiff && absHDiff>flingMin && absVelocityX>velocityMin){
				//ignore horizontal fling
				return false;
			}
			else if(absVDiff>flingMin && absVelocityY>velocityMin){
				if(verticalDiff<0) //fling up
				{
					if(aPagerActivity.aBar.isShowing())
						aPagerActivity.aBar.hide();
				}
				else
				{
					if(!aPagerActivity.aBar.isShowing())
					{
						aPagerActivity.aBar.show();
						aPagerActivity.autoHideActionBar();
					}
				}
			}
			else
				return false;
						
			return true;
		}

		//offset results
		private float flingMin = 100;
		private float velocityMin = 100;

/*       
      @Override
      public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
          try {
          	Log.d("onFling","choha!");
              if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                  return false;
              // right to left swipe
              if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                  Toast.makeText(aPagerActivity, "Left Swipe", Toast.LENGTH_SHORT).show();   
                  Log.d("Fling", "Left");
              }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                  Toast.makeText(aPagerActivity, "Right Swipe", Toast.LENGTH_SHORT).show();
                  Log.d("Fling", "Right");
              } else
              	return false;
              
          } catch (Exception e) {
              // nothing
          }
          return true;
      }
*/       
  }
}//class

class HighlightRect{
	float x,y;
	int width;
	static final int HEIGHT = 110;
	
	public HighlightRect(float clickX, float clickY, int width)
	{
		x = (clickX - width + 225/4);
		y = (clickY - HEIGHT/2);
		
		this.width = width;
	}
		
	public HighlightRect()
	{
		
	}
	
	public String getRect()
	{
		return "<rect height=\""+HEIGHT+"\" width=\""+width+"\" y=\""+(y/1.3333f)+"\" x=\""+(x/1.3333f)+"\" fill=\"#FF0\"/>";
	}
	
	public boolean contains(float px, float py)
	{
		return (px >=x && px <= (x+width) && py >= y && py <= (y+HEIGHT));
	}
	
	@Override
	public String toString()
	{
		return x+","+y+","+width;
	}
	
	public static HighlightRect fromString(String s)
	{
		String a[] = s.split(",");
		HighlightRect h = new HighlightRect();
		h.x = Float.valueOf(a[0]);
		h.y = Float.valueOf(a[1]);
		h.width = Integer.valueOf(a[2]);
		return h;
	}
}