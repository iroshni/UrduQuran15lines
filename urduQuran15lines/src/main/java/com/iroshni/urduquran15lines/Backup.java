package com.iroshni.urduquran15lines;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupManager;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;

public class Backup extends BackupAgentHelper {

   static final String PREFS_BACKUP_KEY = "backup";
   
   @Override
   public void onCreate() {
	  
	   SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this, "AwesomePagerActivity_prefs");
	   addHelper(PREFS_BACKUP_KEY, helper);	      
   }
   
}