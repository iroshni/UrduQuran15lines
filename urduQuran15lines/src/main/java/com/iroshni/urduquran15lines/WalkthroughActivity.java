package com.iroshni.urduquran15lines;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WalkthroughActivity extends Activity {

    private static final int MAX_VIEWS = 2;

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        setContentView(R.layout.walkthrough_activity);
        Button b = (Button) findViewById(R.id.screen_navigation_button);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(new WalkthroughPagerAdapter());
        mViewPager.addOnPageChangeListener(new WalkthroughPageChangeListener(b));
    }

    public void next(View v)
    {
    	if(mViewPager.getCurrentItem() == 0)
    	{
    		mViewPager.setCurrentItem(1);
    	}
    	else
    		finish();
    	
    }


    class WalkthroughPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return MAX_VIEWS;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (View) object;
        }

        @Override
        public Object instantiateItem(View container, int position) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View imageViewContainer = inflater.inflate(R.layout.walkthrough_single_view, null);
            ImageView imageView = (ImageView) imageViewContainer.findViewById(R.id.image_view);

            switch(position) {
            case 0:
                imageView.setImageResource(R.drawable.slide1);
                break;

            case 1:
                imageView.setImageResource(R.drawable.slide2);
                break;
            }

            ((ViewPager) container).addView(imageViewContainer, 0);
            return imageViewContainer;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager)container).removeView((View)object);
        }
        
    }


    class WalkthroughPageChangeListener implements ViewPager.OnPageChangeListener {
    	
    	Button button;
    	public WalkthroughPageChangeListener(Button b)
    	{
    		button = b;
    	}

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageSelected(int position) {
            // Here is where you should show change the view of page indicator
            switch(position) {
            
            case 0:
            	button.setText("Next");
            	break;            	

            case 1:
            	button.setText("End");
                break;

            default:

            }

        }

    }
}